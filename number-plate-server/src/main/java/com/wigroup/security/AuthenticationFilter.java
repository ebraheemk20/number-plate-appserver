package com.wigroup.security;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;
import com.auth0.jwt.internal.org.apache.commons.codec.binary.Base64;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wigroup.common.application.MessageDto;
import com.wigroup.domain.config.constants.ConfigId;
import com.wigroup.domain.config.ConfigService;
import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;
import com.wigroup.utils.ObjectMapperUtil;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;

@WebFilter(filterName = "AuthenticationFilter")
public class AuthenticationFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);
    
    private ObjectMapper MAPPER = ObjectMapperUtil.getObjectMapper();

    @Inject
    private ConfigService configService;

    private static final Set<String> EXCLUDED_PATHS = new HashSet<>(
            Arrays.asList("/rest/login", "/rest/admin/", "/rest/user/register", "/ArquillianServletRunner",
                    "/rest/user/register/otp/validate", "/rest/user/register/otp", "/rest/user/register/pin",
                    "/rest/user/register/email/validateEmail", "/rest/login/forgot", "/rest/login/reset", "/rest/v1", "admin/email/send", "/rest/swagger.json", "/rest/swagger.yaml"));

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String path = request.getRequestURI().substring(request.getContextPath().length()).replaceAll("[/]+$", "");

        // only check if not registering or logging in
        if (EXCLUDED_PATHS.contains(path)
                || (path.startsWith("/rest/admin/")
                || path.startsWith("/rest/v1")
                || path.startsWith("/swagger"))) {

            filterChain.doFilter(servletRequest, servletResponse);

        } else {
            
            if ("".equalsIgnoreCase(path)) {
                RequestDispatcher dispatcher = servletRequest.getRequestDispatcher("index.html");
                dispatcher.forward(servletRequest, servletResponse);
            }

            Map<String, Object> decodedPayload = new HashMap<>();
            String token = request.getHeader("Authorization-X");

            if (token == null || token.isEmpty()) {
                response.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());
                response.setContentType("application/json");
                MessageDto message = new MessageDto("100", "Token missing", 401);
                returnJsonResponse(response, message);
                return;
            }

            try {
                String[] pieces = token.split("\\.");

                if (pieces.length != 3) {
                    response.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());
                    response.setContentType("application/json");
                    MessageDto message = new MessageDto("101", "Token invalid", 401);
                    returnJsonResponse(response, message);
                    return;
                }

                if (!Base64.isArrayByteBase64(pieces[1].getBytes())) {
                    response.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());
                    response.setContentType("application/json");
                    MessageDto message = new MessageDto("101", "Token invalid", 401);
                    returnJsonResponse(response, message);
                    return;
                }

                Base64 decoder = new Base64(true);
                String jsonString = new String(decoder.decodeBase64(pieces[1]), "UTF-8");
                String identifier = null;
                try {
                    JSONObject json = new JSONObject(jsonString);
                    //the identifier is the mobile number if from the mobile, 
                    //or the username if from the admin portal
                    identifier = json.getString("identifier");
                    request.setAttribute("identifier", identifier);
                    request.setAttribute("mobile", identifier);
                } catch (JSONException e) {
                    response.setStatus(Response.Status.BAD_REQUEST.getStatusCode());
                    logger.error("Error accessing jwt payload" + e);
                    response.setContentType("application/json");
                    MessageDto message = new MessageDto("102", "Error accessing jwt payload");
                    returnJsonResponse(response, message);
                    return;
                }

                decodedPayload = new JWTVerifier(JWTUtils.SECRET).verify(token);

                filterChain.doFilter(servletRequest, servletResponse);

                if (!path.contains("/rest/otp/validate")) {
                    Integer timeout = configService.getInt(ConfigId.SESSION_TIMEOUT_SECONDS);
                    response.setHeader("Authorization-X", JWTUtils.sign(timeout, identifier));
                }

            } catch (JWTVerifyException e) {
                logger.error("JWT has expired", e);
                response.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());
                response.setContentType("application/json");
                MessageDto message = new MessageDto("104", "Session has expired", 401);
                returnJsonResponse(response, message);
                return;
            } catch (IllegalStateException | GeneralSecurityException | JsonProcessingException e) {
                response.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());
                logger.error("Invalid Authorization-X header!", e);
                response.setContentType("application/json");
                MessageDto message = new MessageDto("103", "Invalid Authorization-X header!", 401);
                returnJsonResponse(response, message);
                return;
            } catch (IOException e) {
                response.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());
                logger.error("Invalid Authorization-X header!", e);
                response.setContentType("application/json");
                MessageDto message = new MessageDto("103", "Invalid Authorization-X header!", 401);
                returnJsonResponse(response, message);
                return;
            }
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.debug("AuthenticationFilter being created");
    }

    @Override
    public void destroy() {
    }
    
    private void returnJsonResponse(HttpServletResponse response, MessageDto message) throws JsonProcessingException, IOException {
        String jsonMessage = MAPPER.writeValueAsString(message);
        try (PrintWriter writter = response.getWriter()) {
            writter.print(jsonMessage);
        }
    }

}

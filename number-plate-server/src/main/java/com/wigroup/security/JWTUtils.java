package com.wigroup.security;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTSigner.Options;

public class JWTUtils {

    //TODO: remove this?
    public static final String SECRET = "ea2123ac8d0ec27895f2";

    /**
     * Generates secret from application GUID. returns the secret used to
     * decrypt the jwt payload.
     */
    public static String generateSecret(String appGuid) {

        List<String> tokens = Arrays.asList(appGuid.split("-"));
        Collections.reverse(tokens);

        String secret = "";

        for (String token : tokens) {
            char[] chars = token.toCharArray();
            for (int i = 0; i < token.length(); i++) {
                if (i % 2 == 0) {
                    secret = secret.concat("" + chars[i]);
                }
            }
        }
        return secret;
    }

    public static String sign(Integer expirySeconds) {
        return sign(expirySeconds, null);
    }

    public static String sign(Integer expirySeconds, String identifier) {

        Options options = new Options();
        if (expirySeconds != null) {
            options.setExpirySeconds(expirySeconds);
        }

        Map<String, Object> payload = new HashMap<>();
        payload.put("identifier", identifier);

        JWTSigner signer = new JWTSigner(SECRET);

        String token = signer.sign(payload, options);
        return token;

    }

}

package com.wigroup.application;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.wigroup.common.exception.AlreadyExistsException;
import com.wigroup.common.exception.ApplicationException;
import com.wigroup.common.exception.AuthenticationException;
import com.wigroup.common.exception.AuthorizationException;
import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;

@Provider
public class RestApplicationExceptionHandler implements ExceptionMapper<ApplicationException> {

    private Logger logger = LoggerFactory.getLogger(RestApplicationExceptionHandler.class);

    @Override
    public Response toResponse(ApplicationException exception) {

        logger.error(exception);

        Integer statusCode = null;
        GenericResponse response = null;
        response = new GenericResponse(exception.getOtherMessage());
        statusCode = exception.getOtherMessage().getHttpStatusCode() != null ? exception.getOtherMessage().getHttpStatusCode() : null;

        if (statusCode != null) {
            return Response.status(statusCode).entity(response).build();
        }

        if (exception.getClass() == AuthenticationException.class) {
            return Response.status(HttpServletResponse.SC_UNAUTHORIZED).entity(response).build();
        } else if (exception.getClass() == AuthorizationException.class) {
            return Response.status(HttpServletResponse.SC_FORBIDDEN).entity(response).build();
        } else if (exception.getClass() == AlreadyExistsException.class) {
            return Response.status(HttpServletResponse.SC_CONFLICT).entity(response).build();
        }

        return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity(response).build();
    }

}

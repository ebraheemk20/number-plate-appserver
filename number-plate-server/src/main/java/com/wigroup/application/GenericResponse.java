package com.wigroup.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.wigroup.common.application.MessageDto;

@XmlRootElement(name = "GenericResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class GenericResponse {

    private String code;
    private String message;

    public GenericResponse() {
    }

    public GenericResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public GenericResponse(MessageDto messageDto) {
        this.message = messageDto.getMessage();
        this.code = messageDto.getCode();
    }

    public GenericResponse(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

package com.wigroup.application.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.wigroup.application.api.constants.ApiId;
import com.wigroup.infrastructure.persistence.Auditable;

@Entity
@Table(name = "api")
@SuppressWarnings("serial")
public class Api extends Auditable {

    @Id
    @Column
    @Enumerated(EnumType.STRING)
    private ApiId id;

    @Column(name = "api_key")
    private String apiKey;

    /**
     * Required by hibernate.
     */
    protected Api() {
        super();
    }

    // ---- Override equals() and hashcode() ---- //
    @Override
    public int hashCode() {
        return id.hashCode() * 13;
    }

    @Override
    public boolean equals(Object other) {

        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Auditable persistable = (Auditable) other;
        if (id == null || !id.equals(persistable.getId())) {
            return false;
        }
        return true;
    }

    @Override
    public ApiId getId() {
        return id;
    }

    /**
     * Used by hibernate to set the ID, hence private.
     */
    @SuppressWarnings("unused")
    private void setId(ApiId id) {
        this.id = id;
    }

    public String getApiKey() {
        return apiKey;
    }

    @SuppressWarnings("unused")
    private void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

}

package com.wigroup.application.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import com.wigroup.application.api.constants.ApiId;
import com.wigroup.application.api.entity.Api;
import com.wigroup.common.application.MessageDto;
import com.wigroup.common.exception.ApplicationException;
import com.wigroup.common.exception.AuthenticationException;
import com.wigroup.domain.config.ConfigService;
import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;
import com.wigroup.domain.message.MessageService;
import com.wigroup.domain.message.constants.MessageId;
import com.wigroup.infrastructure.persistence.CrudService;

@Singleton
@Startup
@Lock(LockType.READ)
@AccessTimeout(unit = TimeUnit.SECONDS, value = 5)
public class ApiService {

    private static Logger logger = LoggerFactory.getLogger(ConfigService.class);
    private static final Map<ApiId, String> apiCache = new HashMap<>();

    @Inject
    private MessageService messageService;

    @Inject
    private CrudService crudService;

    @PostConstruct
    @Lock(LockType.WRITE)
    @AccessTimeout(unit = TimeUnit.SECONDS, value = 15)
    public void init() {
        loadApis();
    }

    private void loadApis() {
        List<Api> apis = crudService.findAll(Api.class);

        if (!apis.isEmpty()) {
            Map<ApiId, String> apisMap = apis.stream().collect(Collectors.toMap(Api::getId, api -> api.getApiKey()));
            apiCache.putAll(apisMap);
        }
    }

    public void validateApiKey(String apiKey) {
        if (apiKey == null || apiKey.isEmpty()) {
            throw new ApplicationException("Missing api key", messageService.getMessage(MessageId.LOGGED_IN)); //ClientMessage.MISSING_API_KEY
        }

        if (!apiCache.containsValue(apiKey)) {
            throw new AuthenticationException("API key not found [" + apiKey + "]", new MessageDto("100", "INVALID_API_KEY")); //ClientMessage.INVALID_API_KEY
        }
    }

    public void validateAdminApiKey(String apiKey) {
        if (apiKey == null || apiKey.isEmpty()) {
            throw new ApplicationException("Missing api key", messageService.getMessage(MessageId.LOGGED_IN));
        }

        String adminApiKey = apiCache.get(ApiId.ADMIN);

        if (!adminApiKey.equalsIgnoreCase(apiKey)) {
            throw new ApplicationException("Admin invalid api key: [" + apiKey + "]", new MessageDto("-2", "There was an error."));
        }
    }

}

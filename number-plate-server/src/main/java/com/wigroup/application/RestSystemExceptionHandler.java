package com.wigroup.application;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.wigroup.common.exception.GatewayErrorException;
import com.wigroup.common.exception.SystemException;
import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;

@Provider
public class RestSystemExceptionHandler implements ExceptionMapper<SystemException> {

    private Logger logger = LoggerFactory.getLogger(RestSystemExceptionHandler.class);

    @Override
    public Response toResponse(SystemException exception) {

        logger.error(exception.getMessage(), exception);

        if (exception.getClass() == GatewayErrorException.class) {
            return Response.status(HttpServletResponse.SC_BAD_GATEWAY).build();
        }

        return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build();
    }

}

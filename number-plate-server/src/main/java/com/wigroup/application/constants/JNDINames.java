package com.wigroup.application.constants;

public class JNDINames {

    public static final String AUDIT_API_MESSAGE_QUEUE = "java:global/jms/auditApiMessageQueue";
    public static final String AUDIT_API_GATEWAY_MESSAGE_QUEUE = "java:global/jms/auditApiGatewayMessageQueue";

    public static final String SMS_MESSAGE_QUEUE = "java:global/jms/smsMessageQueue";
    //public static final String BILLERS_CACHE = "java:jboss/infinispan/container/billersCache"; //TODO: use this for something else?
    public static final String CREATE_ENDPOINT_MESSAGE_QUEUE = "java:global/jms/deviceEndpointMessageQueue";
    public static final String ADMIN_MESSAGE_QUEUE = "java:global/jms/adminMessageQueue";
    public static final String USER_UPDATE_MESSAGE_QUEUE = "java:global/jms/userUpdateMessageQueue";
}

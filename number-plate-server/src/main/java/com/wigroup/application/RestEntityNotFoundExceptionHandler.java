package com.wigroup.application;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;

@Provider
public class RestEntityNotFoundExceptionHandler implements ExceptionMapper<EntityNotFoundException> {

    private Logger logger = LoggerFactory.getLogger(RestEntityNotFoundExceptionHandler.class);

    @Override
    public Response toResponse(EntityNotFoundException exception) {

        logger.error(exception.getMessage(), exception);
        return Response.status(HttpServletResponse.SC_NOT_FOUND).build();
    }

}

package com.wigroup.application;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;

/**
 * Quick way to access CDI beans
 */
public abstract class ApplicationContext {

    @SuppressWarnings("unchecked")
    public static <T> T getBean(Class<T> beanClass) {
        BeanManager bm = CDI.current().getBeanManager();

        Bean<T> bn = (Bean<T>) bm.getBeans(beanClass).iterator().next();
        CreationalContext<T> ctx = bm.createCreationalContext(bn);
        T bean = (T) bm.getReference(bn, beanClass, ctx);
        return bean;
    }

}

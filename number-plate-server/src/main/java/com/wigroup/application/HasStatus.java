package com.wigroup.application;

public interface HasStatus {

    Object getStatus();

}

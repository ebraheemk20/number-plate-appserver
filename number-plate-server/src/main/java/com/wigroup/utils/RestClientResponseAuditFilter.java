package com.wigroup.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import javax.inject.Inject;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;

import org.apache.commons.compress.utils.IOUtils;
import org.jboss.logging.Logger;

import com.wigroup.domain.audit.AuditApiGatewayMessageSender;
import com.wigroup.domain.audit.entity.AuditTrailGateway;
import java.io.ByteArrayInputStream;
import javax.ejb.Stateless;

public class RestClientResponseAuditFilter implements ClientResponseFilter {

    @Inject
    private AuditApiGatewayMessageSender auditGatewaySender;

    private static final String EXCLUDE_PATH = "/test/rest";
    private static final Logger LOG = Logger.getLogger(RestClientResponseAuditFilter.class);

    @Override
    public void filter(ClientRequestContext request, ClientResponseContext response) throws IOException {

        String path = request.getUri().getPath();
        if (path != null && !path.contains(EXCLUDE_PATH)) {

            LOG.debug("RestClientResponseAuditFilter being applied");

            String requestUUID = request.getHeaderString("apiAuditUUID");
            String gateway = request.getHeaderString("gatewayName");
            response.getHeaders().add("apiAuditUUID", requestUUID);
            String method = request.getMethod();

            int response_status = response.getStatus();

            String responseBody = null;
            if (response.hasEntity() && response.getStatus() == 200) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                InputStream in = response.getEntityStream();
                IOUtils.copy(in, out);
                byte[] data = out.toByteArray();
                if (data.length == 0) {
                    responseBody = "";
                } else {
                    responseBody = new String(data, Charset.forName("UTF-8"));
                }
                response.setEntityStream(new ByteArrayInputStream(data));
            }

            AuditTrailGateway logData = new AuditTrailGateway(gateway, method, "", path, responseBody,
                    response_status, requestUUID);
            auditGatewaySender.auditApiGatewayRequest(logData);
        }
    }
}

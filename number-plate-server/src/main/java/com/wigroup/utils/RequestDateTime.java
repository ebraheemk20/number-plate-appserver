package com.wigroup.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RequestDateTime {

    private static final String timeformat = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final DateFormat formatter = new SimpleDateFormat(timeformat);

    private RequestDateTime() {
    }

    public static String getRequestTime() {

        return formatter.format(new Date());
    }
}

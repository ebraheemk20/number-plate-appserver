package com.wigroup.utils;

import java.util.HashMap;
import java.util.Map;

public class RequestHeaderBuilder {

    Map<String, String> headers = new HashMap<>();
    private boolean invalidSignature = false;

    public RequestHeaderBuilder() {
    }

    public RequestHeaderBuilder with(String name, String value) {
        headers.put(name, value);
        return this;
    }

    public RequestHeaderBuilder withInvalidSignature() {
        invalidSignature = true;
        return this;
    }

    public RequestHeaderFilter build() {

//		headers.put("Authorization-X", sign());
        return new RequestHeaderFilter(headers);

    }

//	private String sign() {
//		
//		Map<String, Object> payload = new HashMap<>();
//		
//		if (guid != null) {
//			payload.put("GUID", guid);
//		}
//		
//		JWTSigner signer = null;
//		
//		if (invalidSignature || guid == null) {
//			signer = new JWTSigner("asdfqwer1234asf");
//		} else {
//			signer = new JWTSigner(JWTUtils.generateSecret(guid));
//		}
//		
//		String token = signer.sign(payload);
//		return token;
//		
//	}
}

package com.wigroup.utils;

import java.io.IOException;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

import org.jboss.logging.Logger;

import com.wigroup.domain.audit.AuditApiGatewayMessageSender;

public class RestClientRequestAuditDecorator implements ClientRequestFilter {

    private static final Logger LOG = Logger.getLogger(RestClientRequestAuditDecorator.class);
    private String gateway;
    private static final String EXCLUDE_PATH = "/test/rest";

    public RestClientRequestAuditDecorator(String gatewayName) {
        this.gateway = gatewayName;
    }

    @Override
    public void filter(ClientRequestContext request) throws IOException {

        String path = request.getUri().getPath();
        if (path != null && !path.contains(EXCLUDE_PATH)) {
            String apiAuditUUID = UUID.randomUUID().toString();
            request.getHeaders().add("apiAuditUUID", apiAuditUUID);
            request.getHeaders().add("gatewayName", gateway);
        }
    }
}

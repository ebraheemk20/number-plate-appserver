package com.wigroup.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.wigroup.application.ApplicationContext;
import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;
import com.wigroup.domain.message.MessageService;
import com.wigroup.domain.config.ConfigService;

public class Validate {

    //TODO move this back to ent-common and refactor ent-common
    private static final Logger LOG = LoggerFactory.getLogger(Validate.class);
    private static MessageService messageService;
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
    		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static Pattern pattern;
    private static ConfigService configService;

    static {
    	pattern = Pattern.compile(EMAIL_PATTERN);
    	
        if (configService == null) {
        	configService = ApplicationContext.getBean(ConfigService.class);
        }
    	
        if (messageService == null) {
            messageService = ApplicationContext.getBean(MessageService.class);
        }
    }
    
    /**
     * Checks the validity of a mobile number. Only the digits 0-9 are accepted
     * (i.e. +27... is a non-valid number).
     *
     * @param mobileNumber the mobile number to be tested for validation.
     * @return true if the mobileNumber is valid.
     */
    public static boolean isMobileNumber(String mobileNumber) {

        if (mobileNumber == null) {
            LOG.debug("mobile number is null");
            return false;
        }

        String mobileNumberTemp = mobileNumber.replaceAll("[^0-9]+", "");
        if (mobileNumber.length() != mobileNumberTemp.length()) {
            LOG.error("mobile number contains invalid characters");
            return false;
        }

        // switch between countries (to be expanded/revisted)
        // default to RSA
        return isValidRSA(mobileNumber);
    }

    public static boolean isValidLandLine(String landlineNumber) {

        if (StringUtils.isBlank(landlineNumber)) {
            return false;
        }

        return isValidRSA(landlineNumber);
    }

    private static boolean isValidRSA(String mobileNumber) {

        String mobileNumberTemp = mobileNumber.replaceAll("[^0-9]+", "");

        if (mobileNumberTemp.length() < 10 || mobileNumberTemp.length() > 11) {
            LOG.debug("mobile number length invalid");
            return false;
        }

        if (mobileNumberTemp.length() == 10 && !mobileNumberTemp.startsWith("0")) {
            LOG.debug("10 digit mobile number must start with 0");
            return false;
        }

        if (mobileNumberTemp.length()
                == 11 && !mobileNumberTemp.startsWith("27")) {
            LOG.debug("11 digit mobile number must start with 27");
            return false;
        }

        return true;
    }

    //--------------------------------------------------------------------------
    // Sanitize mobile number 
    //--------------------------------------------------------------------------
    public static String sanitizeMobileNumber(String number) {
        if (number == null) {
            return null;
        }

        number = number.replaceAll("[^0-9]+", "");

        if (number.startsWith("0")) {
            number = "27" + number.substring(1);
        }
        return number;
    }


    //--------------------------------------------------------------------------
    // Checks a string for any special characters
    //--------------------------------------------------------------------------
    public static boolean checkSpecialCharacters(String string) {

        Pattern p = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = p.matcher(string);
        return m.find();
    }
    
    
}

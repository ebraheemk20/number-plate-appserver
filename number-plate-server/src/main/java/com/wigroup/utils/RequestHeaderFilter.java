package com.wigroup.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

public class RequestHeaderFilter implements ClientRequestFilter {

    Map<String, String> headers = new HashMap<>();

    public RequestHeaderFilter(Map<String, String> headers) {
        this.headers = headers;
    }

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {

        headers.forEach((k, v) -> {
            requestContext.getHeaders().add(k, v);
        });

    }

}

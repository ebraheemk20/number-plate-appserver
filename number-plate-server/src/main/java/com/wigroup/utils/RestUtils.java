package com.wigroup.utils;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;

public class RestUtils {

    private Client client;
    private ResteasyClient restClient;
    private static final Logger LOG = LoggerFactory.getLogger(RestUtils.class);

    public RestUtils(String gatewayName, RestClientResponseAuditFilter responseFilter) {
        client = ClientBuilder.newClient().register(new RestClientRequestAuditDecorator(gatewayName)).register(responseFilter);
        restClient = new ResteasyClientBuilder().build().register(new RestClientRequestAuditDecorator(gatewayName)).register(responseFilter);
    }

    /* Returns the entire response should you need to check statuses */
    public Response getResponse(String baseURI, String path, MultivaluedMap<String, Object> headers) {

        return getWebTarget(baseURI, path).request(MediaType.APPLICATION_JSON).headers(headers).get();
    }

    public Response asyncGetResponse(String baseURI, String path, MultivaluedMap<String, Object> headers) throws InterruptedException, ExecutionException {

        Future<Response> response = getWebTarget(baseURI, path).request(MediaType.APPLICATION_JSON).headers(headers).async().get();
        return response.get();
    }

    public Response get(String baseUrl, String path, MultivaluedMap<String, Object> queryParams) {

        return getRestWebTarget(baseUrl, path).queryParams(queryParams).request(MediaType.APPLICATION_JSON).get();
    }

    public Object postRequest(String baseUrl, String path, Object requestEntity) {

        WebTarget target = getWebTarget(baseUrl, path);
        Response response = target.request().accept(MediaType.APPLICATION_JSON).post(Entity.json(requestEntity));

        return returnObject(response);
    }

    /*
	 * Used for initial login to an external api, has no hearders in the request
     */
    public Object postFormRequest(String baseURI, String path, MultivaluedMap<String, String> formParams) {

        Response response = getWebTarget(baseURI, path).request(MediaType.APPLICATION_JSON)
                .post(Entity.form(formParams));

        return returnObject(response);
    }

    public Response postForm(String baseURI, String path, Form form) {

        return getWebTarget(baseURI, path).request(MediaType.APPLICATION_JSON).post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED));
    }

    public Response post(String baseURI, String path, MultivaluedMap<String, String> formParams) {

        return getWebTarget(baseURI, path).request(MediaType.APPLICATION_JSON).post(Entity.form(formParams));
    }

    public Response postJson(String baseURI, String path, Object request) {

        Response response = null;
        LOG.info("request: " + request);
        try {
            return getWebTarget(baseURI, path).request().accept(MediaType.APPLICATION_JSON).post(Entity.json(request));
        } catch (Exception e) {
            LOG.debug("Could not connect to post " + e);
        }
        return response;
    }

    public Object postFormWithHeaders(String baseURI, String path, MultivaluedMap<String, String> formParams,
            MultivaluedMap<String, Object> headers) {

        Response response = getWebTarget(baseURI, path).request(MediaType.APPLICATION_JSON).headers(headers)
                .post(Entity.form(formParams));

        return returnObject(response);
    }

    public Object postWithHeaders(String baseURI, String path, Object requestEntity,
            MultivaluedMap<String, Object> headers) {

        Response response = getWebTarget(baseURI, path).request(MediaType.APPLICATION_JSON).headers(headers)
                .post(Entity.entity(requestEntity, MediaType.APPLICATION_JSON));

        return returnObject(response);
    }

    public Response post(String baseURI, String path, Object requestEntity, MultivaluedMap<String, Object> headers) {

        return getWebTarget(baseURI, path).request(MediaType.APPLICATION_JSON).headers(headers)
                .post(Entity.entity(requestEntity, MediaType.APPLICATION_JSON));
    }

    public Response asyncPost(String baseURI, String path, Object requestEntity, MultivaluedMap<String, Object> headers) throws InterruptedException, ExecutionException {

        Future<Response> response = getWebTarget(baseURI, path).request(MediaType.APPLICATION_JSON).headers(headers).async()
                .post(Entity.entity(requestEntity, MediaType.APPLICATION_JSON));

        return response.get();
    }

    public Object putWithHeaders(String baseURI, String path, Object requestEntity, String mediaType,
            MultivaluedMap<String, Object> headers) {

        Response response = getWebTarget(baseURI, path).request(MediaType.APPLICATION_JSON).headers(headers)
                .put(Entity.entity(requestEntity, MediaType.APPLICATION_JSON));

        return returnObject(response);
    }

    public Response put(String baseURI, String path, Object requestEntity, String mediaType,
            MultivaluedMap<String, Object> headers) {

        return getWebTarget(baseURI, path).request(MediaType.APPLICATION_JSON).headers(headers)
                .put(Entity.entity(requestEntity, MediaType.APPLICATION_JSON));
    }

    public Response put(String baseURI, String path, Object requestEntity) {

        return getWebTarget(baseURI, path).request().accept(MediaType.APPLICATION_JSON)
                .put(Entity.json(requestEntity));
    }

    public Response asyncPut(String baseURI, String path, Object requestEntity, String mediaType,
            MultivaluedMap<String, Object> headers) throws InterruptedException, ExecutionException {

        return getWebTarget(baseURI, path).request(MediaType.APPLICATION_JSON).headers(headers).async()
                .put(Entity.entity(requestEntity, MediaType.APPLICATION_JSON)).get();
    }

    public Object deleteWithHeaders(String baseURI, String path, Object requestEntity,
            MultivaluedMap<String, Object> headers) {

        Response response = (Response) getWebTarget(baseURI, path).request().headers(headers)
                .delete(requestEntity.getClass());

        return returnObject(response);
    }

    public Object deleteWithHeaders(String baseURI, String path, MultivaluedMap<String, Object> headers) {

        Response response = (Response) getWebTarget(baseURI, path).request().headers(headers).delete();

        return returnObject(response);
    }

    public Response delete(String baseURI, String path, MultivaluedMap<String, Object> headers) {

        return getWebTarget(baseURI, path).request().headers(headers).delete();
    }

    public Response asyncDelete(String baseURI, String path, MultivaluedMap<String, Object> headers) throws InterruptedException, ExecutionException {

        return getWebTarget(baseURI, path).request().headers(headers).async().delete().get();
    }

    public void delete(String baseURI, String path, Object requestEntity, MultivaluedMap<String, Object> headers) {

        getWebTarget(baseURI, path).request().headers(headers).delete(requestEntity.getClass());
    }

    private WebTarget getWebTarget(String baseUrl, String path) {

        if (StringUtils.isEmpty(path)) {
            return getWebTarget(baseUrl);
        } else {
            return client.target(baseUrl).path(path);
        }
    }

    private WebTarget getWebTarget(String baseUrl) {

        return client.target(baseUrl);
    }

    private ResteasyWebTarget getRestWebTarget(String baseUrl, String path) {

        if (StringUtils.isEmpty(path)) {
            return getRestWebTarget(baseUrl);
        } else {
            return restClient.target(baseUrl).path(path);
        }
    }

    private ResteasyWebTarget getRestWebTarget(String baseUrl) {

        return restClient.target(baseUrl);
    }

    private Object returnObject(Response response) {
        if (isSuccessRequest(response.getStatus())) {
            Object obj = null;
            if (response.hasEntity()) {
                obj = response.getEntity();
            }

            response.close();
            return obj;
        } else {
            throw new WebApplicationException("Request failed ", response);
        }
    }

    private boolean isSuccessRequest(int statusCode) {

        String errorCode = String.valueOf(statusCode);
        if (errorCode.startsWith("20")) {
            return true;
        }

        return false;
    }

}

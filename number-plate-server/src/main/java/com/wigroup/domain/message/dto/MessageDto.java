package com.wigroup.domain.message.dto;

import com.wigroup.domain.message.entity.Message;

public class MessageDto {

    private String id;
    private String messageCode;
    private Integer httpCode;
    private String message;

    public MessageDto(Message message) {
        this.id = message.getId().toString();
        this.messageCode = message.getCode();
        this.httpCode = message.getHttpStatusCode();
        this.message = message.getMessage();
    }

    private MessageDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

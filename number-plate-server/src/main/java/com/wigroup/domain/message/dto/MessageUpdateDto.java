package com.wigroup.domain.message.dto;

import com.wigroup.domain.message.constants.MessageId;

public class MessageUpdateDto {

    private Integer httpStatusCode;
    private MessageId messageId;
    private String message;

    public MessageUpdateDto(MessageId id, Integer code, String message) {
        this.messageId = id;
        this.httpStatusCode = code;
        this.message = message;
    }

    private MessageUpdateDto() {
    }

    public Integer getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(Integer httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public MessageId getMessageId() {
        return messageId;
    }

    public void setMessageId(MessageId messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

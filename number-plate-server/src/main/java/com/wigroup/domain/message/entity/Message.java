package com.wigroup.domain.message.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.wigroup.domain.message.constants.MessageId;
import com.wigroup.infrastructure.persistence.Auditable;

@Entity
@Table(name = "message")
public class Message extends Auditable {

    @Id
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private MessageId id;

    @Column(nullable = false, length = 755)
    private String message;

    @Column
    private String code;

    @Column(name = "http_status_code", nullable = true)
    private Integer httpStatusCode;

    protected Message() {
    }

    @Override
    public MessageId getId() {
        return id;
    }

    public void setId(MessageId id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(Integer httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }
}

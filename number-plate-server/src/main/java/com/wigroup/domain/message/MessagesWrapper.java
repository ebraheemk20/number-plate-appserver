package com.wigroup.domain.message;

import com.wigroup.domain.message.dto.MessageDto;
import com.wigroup.domain.message.entity.Message;
import java.util.ArrayList;
import java.util.List;

public class MessagesWrapper {

    private List<MessageDto> projectMessage = new ArrayList<>();

    public MessagesWrapper(List<Message> messages) {

        messages.parallelStream().forEach(e -> {
            projectMessage.add(new MessageDto(e));
        });
    }

    private MessagesWrapper() {
    }


    public List<MessageDto> getProjectMessage() {
        return projectMessage;
    }

    public void setProjectMessage(List<MessageDto> projectMessage) {
        this.projectMessage = projectMessage;
    }
}

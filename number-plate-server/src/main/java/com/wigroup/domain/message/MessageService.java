package com.wigroup.domain.message;

import com.wigroup.domain.message.constants.MessageId;
import com.wigroup.domain.message.entity.Message;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import com.wigroup.common.application.MessageDto;
import com.wigroup.common.exception.SystemException;
import com.wigroup.infrastructure.persistence.CrudService;

@Singleton
@Startup
@DependsOn("CrudService")
@Lock(LockType.READ)
@AccessTimeout(unit = TimeUnit.SECONDS, value = 5)
public class MessageService {
    
    //Map for project specific messages
    private static final Map<MessageId, MessageDto> messageCache = new HashMap<>();

    @Inject
    private CrudService crudService;

    @PostConstruct
    @Lock(LockType.WRITE)
    @AccessTimeout(unit = TimeUnit.SECONDS, value = 15)
    public void init() {
        loadSystemMessages();
    }

    public String getString(MessageId id) {

        return getMessage(id).getMessage();
    }

    public String getCode(MessageId id) {

        return getMessage(id).getCode();
    }
    
    //--------------------------------------------------------------------------
    //Get project specific messages from the db
    //--------------------------------------------------------------------------
    public MessageDto getMessage(MessageId id) {

        //If the messageCache does not have the enum in its map then look for it
        //in the db and put it in the map then return the message
        if (!messageCache.containsKey(id)) {
            Message message = crudService.findById(Message.class, id);
            validateNotNull(message, id);
            MessageDto thisMessage = new MessageDto(message.getCode(), message.getMessage(), message.getHttpStatusCode());
            messageCache.put(id, thisMessage);
            return thisMessage;
        } else {
            return messageCache.get(id);
        }
    }

    //--------------------------------------------------------------------------
    // Update project specific message in db and messageCache
    //--------------------------------------------------------------------------
    @Lock(LockType.WRITE)
    public void updateMessage(MessageId id, String value, Integer httpCode) {
        Message message = crudService.findById(Message.class, id);

        if (message != null) {
            String messageValue = message.getMessage();
            if (StringUtils.isNotBlank(value)) {
                message.setMessage(value);
                messageValue = value;
            }
            message.setHttpStatusCode(httpCode);
            crudService.merge(message);
            messageCache.put(id, new MessageDto(message.getCode(), messageValue, httpCode));
        }
    }

    //--------------------------------------------------------------------------
    // Load the project specific messages into the map
    //--------------------------------------------------------------------------
    private void loadSystemMessages() {

        if (messageCache.isEmpty()) {
            List<Message> messages = crudService.findAll(Message.class);

            if (!messages.isEmpty()) {
                Map<MessageId, Message> messagesMap = messages.stream().collect(Collectors.toMap(Message::getId, message -> message));

                Arrays.stream(MessageId.values()).forEach(message -> {
                    Message thisMessage = messagesMap.get(message);
                    validateNotNull(thisMessage, message);
                    messageCache.put(message, new MessageDto(thisMessage.getCode(), thisMessage.getMessage(), thisMessage.getHttpStatusCode()));
                });
            }
        }
    }

    private void validateNotNull(Message message, MessageId id) {
        if (message == null) {
            throw new SystemException("No property found for message [" + id.name() + "]");
        }
    }

}

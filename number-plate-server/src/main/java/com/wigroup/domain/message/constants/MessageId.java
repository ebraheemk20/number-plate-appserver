package com.wigroup.domain.message.constants;

public enum MessageId {

    FIELD_LENGTH,
    LOGGED_IN;
}

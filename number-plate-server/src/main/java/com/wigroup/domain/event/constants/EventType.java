package com.wigroup.domain.event.constants;

public enum EventType {

    FIRST_NAME_LAST_NAME_CHANGE("De register user with subscribers"),
    EMAIL_CHANGE("Send user verification email"),
    MOBILE_NUMBER_CHANGE("Send otp sms"),
    BILL_SUBSCRIPTION("Verification email must be sent before subscribing to bill"),
    PROCCESS_SUBSCRIPTION("Email validation complete, process subscription to ebpp");

    private final String message;

    private EventType(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}

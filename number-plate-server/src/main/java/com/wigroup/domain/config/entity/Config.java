package com.wigroup.domain.config.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.wigroup.domain.config.constants.ConfigId;
import com.wigroup.domain.config.constants.ConfigType;
import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;
import com.wigroup.infrastructure.persistence.Auditable;

@Entity
@Table(name = "config")
public class Config extends Auditable {

    private static Logger logger = LoggerFactory.getLogger(Config.class);

    @Id
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ConfigId id;

    @Column(nullable = false, length = 755)
    private String value;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ConfigType type;

    /**
     * Required by hibernate.
     */
    protected Config() {
        super();
    }

    public Config(ConfigId id, String value, String description, ConfigType type) {
        super();
        this.id = id;
        this.value = value;
        this.description = description;
        this.type = type;
    }

    // ---- Override equals() and hashcode() ---- //
    @Override
    public int hashCode() {
        return id.hashCode() * 13;
    }

    @Override
    public boolean equals(Object other) {

        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Auditable persistable = (Auditable) other;
        if (id == null || !id.equals(persistable.getId())) {
            return false;
        }
        return true;
    }

    // ---- Business methods ---- //
    @Override
    public ConfigId getId() {
        return id;
    }

    /**
     * Used by hibernate to set the ID, hence private.
     */
    @SuppressWarnings("unused")
    private void setId(ConfigId id) {
        this.id = id;
    }

    public static Logger getLogger() {
        return logger;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public ConfigType getType() {
        return type;
    }

    public void updateValue(String value) {
        this.value = value;
    }

}

package com.wigroup.domain.config;

import com.wigroup.domain.config.constants.ConfigId;
import com.wigroup.domain.config.constants.ConfigType;
import com.wigroup.domain.config.entity.Config;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import com.wigroup.common.exception.SystemException;
import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;
import com.wigroup.infrastructure.persistence.CrudService;

@Singleton
@Startup
@DependsOn("CrudService")
@Lock(LockType.READ)
@AccessTimeout(unit = TimeUnit.SECONDS, value = 5)
public class ConfigService {

    private static Logger logger = LoggerFactory.getLogger(ConfigService.class);
    private static final Map<ConfigId, Object> configCache = new HashMap<>();

    @Inject
    private CrudService crudService;

    @PostConstruct
    @Lock(LockType.WRITE)
    @AccessTimeout(unit = TimeUnit.SECONDS, value = 15)
    public void init() {
        loadSystemConfigs();
    }

    public Double getDouble(ConfigId id) {

        if (!configCache.containsKey(id)) {
            Config config = crudService.findById(Config.class, id);
            validateNotNull(config, id);
            Double value = Double.parseDouble(config.getValue());
            configCache.put(id, value);
            return value;
        } else {
            return (Double) configCache.get(id);
        }

    }

    public String getString(ConfigId id) {

        if (!configCache.containsKey(id)) {
            Config config = crudService.findById(Config.class, id);
            validateNotNull(config, id);
            configCache.put(id, config.getValue());
            return config.getValue();
        } else {
            return (String) configCache.get(id);
        }
    }

    public Boolean getBoolean(ConfigId id) {

        if (!configCache.containsKey(id)) {
            Config config = crudService.findById(Config.class, id);
            validateNotNull(config, id);
            Boolean value = Boolean.parseBoolean(config.getValue());
            configCache.put(id, value);
            return value;
        } else {
            return (Boolean) configCache.get(id);
        }
    }

    public Integer getInt(ConfigId id) {

        if (!configCache.containsKey(id)) {
            Config config = crudService.findById(Config.class, id);
            validateNotNull(config, id);
            Integer value = Integer.parseInt(config.getValue());
            configCache.put(id, value);
            return value;
        } else {
            return (Integer) configCache.get(id);
        }
    }

    public Date getDate(ConfigId id) {

        if (!configCache.containsKey(id)) {
            Config config = crudService.findById(Config.class, id);
            validateNotNull(config, id);

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            Date date = null;
            try {
                date = dateFormat.parse(config.getValue());
            } catch (ParseException e) {
                logger.error("Error creating date from configId: " + id.name(), e);
            }
            configCache.put(id, date);
            return date;
        } else {
            return (Date) configCache.get(id);
        }
    }

    private void validateNotNull(Config config, ConfigId id) {
        if (config == null) {
            throw new SystemException("No property found for config [" + id.name() + "]");
        }

    }

    @Lock(LockType.WRITE)
    public void updateConfig(ConfigId id, String value) {
        Config config = crudService.findById(Config.class, id);
        config.updateValue(value);
        crudService.merge(config);
        configCache.put(id, value);
    }

    private void loadSystemConfigs() {

        if (configCache.isEmpty()) {
            List<Config> configs = crudService.findAll(Config.class);

            if (!configs.isEmpty()) {
                Map<ConfigId, Config> configsMap = configs.stream().collect(Collectors.toMap(Config::getId, con -> con));

                Arrays.stream(ConfigId.values()).forEach(configId -> {
                    Config thisConfig = configsMap.get(configId);
                    validateNotNull(thisConfig, configId);
                    ConfigType type = thisConfig.getType();
                    switch (type) {
                        case STRING:
                            configCache.put(configId, thisConfig.getValue());
                            break;
                        case INTEGER:
                            configCache.put(configId, Integer.parseInt(thisConfig.getValue()));
                            break;
                        case BOOLEAN:
                            configCache.put(configId, Boolean.parseBoolean(thisConfig.getValue()));
                            break;
                        case DOUBLE:
                            configCache.put(configId, Double.parseDouble(thisConfig.getValue()));
                            break;
                        case DATE:
                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                            Date date = null;
                            try {
                                date = dateFormat.parse(thisConfig.getValue());
                            } catch (ParseException e) {
                                logger.error("Error creating date from configId: " + configId.name(), e);
                            }
                            configCache.put(configId, date);
                            break;
                    }
                });
            }
        }
    }
}

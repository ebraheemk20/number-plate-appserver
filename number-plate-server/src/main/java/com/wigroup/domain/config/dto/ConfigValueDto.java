package com.wigroup.domain.config.dto;

public class ConfigValueDto {

    private String value;

    private ConfigValueDto() {
    }

    public ConfigValueDto(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

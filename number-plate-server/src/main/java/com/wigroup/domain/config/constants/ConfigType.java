package com.wigroup.domain.config.constants;

public enum ConfigType {

    STRING, INTEGER, BOOLEAN, DOUBLE, DATE;

}

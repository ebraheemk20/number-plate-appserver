package com.wigroup.domain.audit.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.wigroup.domain.audit.constants.AuditType;
import com.wigroup.infrastructure.persistence.Auditable;

@Entity
@SuppressWarnings("serial")
@Table(name = "audit_trail_api_gateway")
public class AuditTrailGateway extends Auditable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = true)
    private String api_uuid;

    private String gateway;

    @Column(name = "method_name")
    private String method;

    @Column(columnDefinition = "text")
    private String body;

    private String path;

    @Column(columnDefinition = "text")
    private String response_data;

    private int response_status;

    @Enumerated(EnumType.STRING)
    @Column(nullable = true)
    private AuditType type;

    @Transient
    private boolean logRequest;

    protected AuditTrailGateway() {
    }

    public AuditTrailGateway(String requestData, String api_uuid) {
        this.api_uuid = api_uuid;
        this.body = requestData;
        this.logRequest = true;
    }

    public AuditTrailGateway(String gateway, String method, String body, AuditType type) {
        super();
        this.gateway = gateway;
        this.method = method;
        this.body = body;
        this.type = type;
    }

    public AuditTrailGateway(String gateway, String method, String body, String path, String response_data,
            int response_status, String api_uuid) {
        this.gateway = gateway;
        this.method = method;
        this.body = body;
        this.path = path;
        this.response_data = response_data;
        this.response_status = response_status;
        this.api_uuid = api_uuid;
    }

    public Long getId() {
        return id;
    }

    public String getGateway() {
        return gateway;
    }

    public String getMethod() {
        return method;
    }

    public String getBody() {
        return body;
    }

    public AuditType getType() {
        return type;
    }

    @SuppressWarnings("unused")
    private void setId(Long id) {
        this.id = id;
    }

    @SuppressWarnings("unused")
    private void setGateway(String gateway) {
        this.gateway = gateway;
    }

    @SuppressWarnings("unused")
    private void setMethod(String method) {
        this.method = method;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @SuppressWarnings("unused")
    private void setType(AuditType type) {
        this.type = type;
    }

    public String getApi_uuid() {
        return api_uuid;
    }

    public void setApi_uuid(String api_uuid) {
        this.api_uuid = api_uuid;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getResponse_data() {
        return response_data;
    }

    public void setResponse_data(String response_data) {
        this.response_data = response_data;
    }

    public int getResponse_status() {
        return response_status;
    }

    public void setResponse_status(int response_status) {
        this.response_status = response_status;
    }

    public boolean isLogRequest() {
        return logRequest;
    }
}

package com.wigroup.domain.audit.entity;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.wigroup.infrastructure.persistence.Auditable;

@Entity
@SuppressWarnings("serial")
@Table(name = "audit_trail_exception")
public class AuditTrailException extends Auditable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description")
    private String message;

    @Column(name = "stack_trace", columnDefinition = "text")
    private String stacktrace;

    protected AuditTrailException() {
    }

    public AuditTrailException(String message, Throwable throwable) {
        super();
        this.message = message;
        if (throwable != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw, true);
            throwable.printStackTrace(pw);
            this.stacktrace = sw.getBuffer().toString();
        }
    }

    public Long getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getStacktrace() {
        return stacktrace;
    }

    @SuppressWarnings("unused")
    private void setId(Long id) {
        this.id = id;
    }

    @SuppressWarnings("unused")
    private void setMessage(String message) {
        this.message = message;
    }

    @SuppressWarnings("unused")
    private void setStacktrace(String stackTrace) {
        this.stacktrace = stackTrace;
    }

}

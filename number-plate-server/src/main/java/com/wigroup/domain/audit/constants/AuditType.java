package com.wigroup.domain.audit.constants;

public enum AuditType {

    REQUEST,
    RESPONSE;

}

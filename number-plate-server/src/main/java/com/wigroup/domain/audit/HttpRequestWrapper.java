package com.wigroup.domain.audit;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.io.IOUtils;

public class HttpRequestWrapper extends HttpServletRequestWrapper {

    private ByteArrayOutputStream cachedBytes;
    private final Map<String, String> customHeaders;

    public HttpRequestWrapper(HttpServletRequest request) {
        super(request);
        this.customHeaders = new HashMap<>();
    }

    public String getBody() {

        BufferedReader br = null;
        StringBuilder streamContent = new StringBuilder();

        String line;
        try {
            br = new BufferedReader(new InputStreamReader(getInputStream(), Charset.forName("UTF-8")));
            while ((line = br.readLine()) != null) {
                streamContent.append(line);
            }
        } catch (IOException ex) {
        }
        return streamContent.toString();
    }

    public String getHeadersAsString() {
        boolean firstHeaderName = true;

        StringBuilder requestHeader = new StringBuilder();

        requestHeader.append("{");

        for (String headerName : Collections.list(super.getHeaderNames())) {
            boolean firstHeaderValue = true;
            if (firstHeaderName) {
                firstHeaderName = false;
            } else {
                requestHeader.append(",");
            }
            requestHeader.append("\"");
            requestHeader.append(headerName);
            requestHeader.append("\":\"");
            for (String headerElement : Collections.list(super.getHeaders(headerName))) {
                if (firstHeaderValue) {
                    firstHeaderValue = false;
                } else {
                    requestHeader.append(",");
                }
                requestHeader.append(headerElement);
            }
            requestHeader.append("\"");
        }

        requestHeader.append("}");

        return requestHeader.toString();
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        if (cachedBytes == null) {
            cacheInputStream();
        }

        return new CachedServletInputStream();
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(getInputStream(), Charset.forName("UTF-8")));
    }

    private void cacheInputStream() throws IOException {
        /*
		 * Cache the inputstream in order to read it multiple times. For
		 * convenience, I use apache.commons IOUtils
         */
        cachedBytes = new ByteArrayOutputStream();
        IOUtils.copy(super.getInputStream(), cachedBytes);
    }

    public void addHeader(String name, String value) {
        this.customHeaders.put(name, value);
    }

    @Override
    public String getHeader(String name) {

        String headerValue = customHeaders.get(name);

        if (headerValue != null) {
            return headerValue;
        }

        return ((HttpServletRequest) getRequest()).getHeader(name);
    }

    /* An inputstream which reads the cached request body */
    public class CachedServletInputStream extends ServletInputStream {

        private final ByteArrayInputStream input;

        public CachedServletInputStream() {
            /* create a new input stream from the cached request body */
            input = new ByteArrayInputStream(cachedBytes.toByteArray());
        }

        @Override
        public int read() throws IOException {
            return input.read();
        }

        @Override
        public boolean isFinished() {
            throw new UnsupportedOperationException("Not supported yet."); // To
            // change
            // body
            // of
            // generated
            // methods,
            // choose
            // Tools
            // |
            // Templates.
        }

        @Override
        public boolean isReady() {
            throw new UnsupportedOperationException("Not supported yet."); // To
            // change
            // body
            // of
            // generated
            // methods,
            // choose
            // Tools
            // |
            // Templates.
        }

        @Override
        public void setReadListener(ReadListener rl) {
            throw new UnsupportedOperationException("Not supported yet."); // To
            // change
            // body
            // of
            // generated
            // methods,
            // choose
            // Tools
            // |
            // Templates.
        }
    }

}

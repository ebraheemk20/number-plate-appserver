package com.wigroup.domain.audit;

import com.wigroup.domain.audit.HideSensitiveDataEvent.EventType;
import com.wigroup.domain.audit.entity.AuditTrailApi;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wigroup.domain.config.ConfigService;
import com.wigroup.domain.config.constants.ConfigId;
import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;
import java.util.concurrent.TimeUnit;
import javax.enterprise.event.Event;

@WebFilter(filterName = "AuditApiFilter")
public class AuditApiFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(AuditApiFilter.class);

    private static final Set<String> EXCLUDED_PATHS = new HashSet<>(
            Arrays.asList("/admin", "/ArquillianServletRunner", "/rest/swagger.json", "/rest/swagger.yaml"));

    //Note: These are the endpoints where sensitive data will be masked
    private static final Set<String> SENSITIVE_PATHS = new HashSet<>(
            Arrays.asList("/rest/user/register/pin", "/rest/login/reset", "/rest/login", "/rest/pin/update"));
    
    @Inject
    private ConfigService configService;

    @Inject
    private AuditApiMessageSender auditApiSender;
    
    @Inject 
    private Event<HideSensitiveDataEvent> event;

    protected static String hostIp;

    static {
        try {
            InetAddress thisIp = InetAddress.getLocalHost();
            hostIp = thisIp.getHostAddress();
        } catch (UnknownHostException ex) {
            hostIp = "unknown";
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpRequestWrapper httpRequest = new HttpRequestWrapper((HttpServletRequest) servletRequest);
        HttpResponseWrapper httpResponse = new HttpResponseWrapper((HttpServletResponse) servletResponse);

        String path = httpRequest.getRequestURI().substring(httpRequest.getContextPath().length()).replaceAll("[/]+$", "");

        // only check if not registering or logging in
        if (EXCLUDED_PATHS.contains(path)) {

            filterChain.doFilter(servletRequest, servletResponse);

        } else {

            logger.debug("AuditTrailApiFilter being applied");

            String deviceId = httpRequest.getHeader("deviceId");
            String platform = httpRequest.getHeader("platform");
            String osVersion = httpRequest.getHeader("os_version");
            String apiAuditUUID = UUID.randomUUID().toString();
            httpRequest.addHeader("apiAuditUUID", apiAuditUUID);
            String url = httpRequest.getPathInfo();
            // this is the only way we can identify the method called without
            // adding a bunch of stuff to the header
            String method = httpRequest.getMethod();

            if (httpRequest.getQueryString() != null) {
                url = url + "?" + httpRequest.getQueryString();
            }

            Boolean logBody = configService.getBoolean(ConfigId.LOG_RESPONSE_REQUESTS);
            logHttpRequest(httpRequest, deviceId, platform, osVersion, method, logBody, url);

            filterChain.doFilter(httpRequest, httpResponse);

            logHttpResponse(httpResponse, httpRequest, deviceId, platform, osVersion, method, logBody, url);
            
            //Note: If a sensitive path is used, it would fire off an event.
            //      A listner would be waiting / listening for the event.
            if (SENSITIVE_PATHS.contains(path)) {
		event.fire(new HideSensitiveDataEvent(path, apiAuditUUID, EventType.ApiRequestResponse, TimeUnit.SECONDS, 7));
                //Note: Listner in DataEncriptionService class. "captureEvent" method
	    }

        }

    }

    private void logHttpRequest(HttpRequestWrapper httpRequest, String deviceId, String platform, String osVersion,
            String method, Boolean logBody, String url) {

        String requestBody = null;

        if (logBody) {
            requestBody = httpRequest.getBody();
        }

        AuditTrailApi message = new AuditTrailApi(httpRequest.getHeadersAsString(), deviceId, platform, osVersion,
                method, url, requestBody, hostIp, httpRequest.getHeader("apiAuditUUID"));
        auditApiSender.auditApiRequest(message);
    }

    private void logHttpResponse(HttpResponseWrapper httpResponse, HttpRequestWrapper httpRequest, String deviceId, String os, String osVersion,
            String method, Boolean logBody, String url) {

        String responseBody = null;

        if (logBody) {
            if (url.contains("/images/")) {
                responseBody = "coporate image in the response body";
            } else {
                try {
                    responseBody = new String(httpResponse.getCopy(), httpResponse.getCharacterEncoding());
                } catch (UnsupportedEncodingException ex) {
                    logger.error("Unable to access response body during audit trail logging", ex);
                }
            }
        }

        AuditTrailApi message = new AuditTrailApi(httpResponse.getStatus(), deviceId, os, osVersion, method, url,
                responseBody, httpRequest.getHeader("apiAuditUUID"));
        auditApiSender.auditApiRequest(message);

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.debug("AuditTrailApiFilter being created");
    }

    @Override
    public void destroy() {
    }

}

package com.wigroup.domain.audit;

import com.wigroup.domain.audit.entity.AuditTrailApi;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.wigroup.domain.admin.AuditFilter;
import com.wigroup.domain.config.ConfigService;
import com.wigroup.domain.config.constants.ConfigId;
import com.wigroup.infrastructure.Repository;

@Stateless
public class AuditRepository extends Repository {

    @Inject
    private ConfigService configService;

    public List<AuditTrailApi> findAuditTrailApi(AuditFilter filter) {

        // build query string
        StringBuilder query = new StringBuilder();
        query.append("select t from AuditTrailApi t where t.url not like :exclude ");
        if (filter.getApi() != null) {
            query.append("  and t.platform = :apiId ");
        }
        if (filter.getMethodName() != null) {
            query.append("  and method = :method ");
        }
        query.append("  order by id desc ");

        // create query
        TypedQuery<AuditTrailApi> typedQuery = entityManager.createQuery(query.toString(), AuditTrailApi.class);
        typedQuery.setParameter("exclude", "\\/admin%");
        if (filter.getApi() != null) {
            typedQuery.setParameter("apiId", filter.getApi());
        }
        if (filter.getMethodName() != null) {
            typedQuery.setParameter("method", filter.getMethodName());
        }

        // add paging params
        typedQuery.setFirstResult(filter.getPageOffset() * filter.getPageSize());
        typedQuery.setMaxResults(filter.getPageSize());

        List<AuditTrailApi> results = typedQuery.getResultList();

        return results;

    }

    public void listOverDueBills() {

    }

    public void clearLogs() {

        Integer interval = configService.getInt(ConfigId.AUDIT_TRAIL_RETENTION_MONTH);

        // clear audit_trail_api
        StringBuilder query = new StringBuilder();
        query.append(" UPDATE audit_trail_api SET response_data = NULL ");
        query.append(" WHERE DATE_ADD(date_created, INTERVAL :interval MONTH) < NOW() ");

        Query nativeQuery = entityManager.createNativeQuery(query.toString());
        nativeQuery.setParameter("interval", interval);

        nativeQuery.executeUpdate();

        // clear audit_trail_api_gateway
        query = new StringBuilder();
        query.append(" UPDATE audit_trail_api_gateway SET response_data = NULL ");
        query.append(" WHERE DATE_ADD(date_created, INTERVAL :interval MONTH) < NOW()");
        nativeQuery = entityManager.createNativeQuery(query.toString());
        nativeQuery.setParameter("interval", interval);

        nativeQuery.executeUpdate();

        // clear audit_trail_service_gateway
        query = new StringBuilder();
        query.append(" UPDATE audit_trail_exception SET stack_trace = NULL ");
        query.append(" WHERE DATE_ADD(date_created, INTERVAL :interval MONTH) < NOW() ");
        nativeQuery = entityManager.createNativeQuery(query.toString());
        nativeQuery.setParameter("interval", interval);

        nativeQuery.executeUpdate();

    }

}

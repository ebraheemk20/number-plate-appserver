package com.wigroup.domain.audit;

import com.wigroup.domain.audit.constants.AuditType;
import com.wigroup.domain.audit.entity.AuditTrailGateway;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.wigroup.common.gateway.LoggableGatewayRequest;

@AuditApiGateway
@Interceptor
public class AuditApiGatewayInterceptor {

    @Inject
    private AuditApiGatewayMessageSender auditGatewaySender;

    @AroundInvoke
    public Object logAuditGateway(InvocationContext invocationContext) throws Exception {

        Object[] params = invocationContext.getParameters();
        LoggableGatewayRequest request = (LoggableGatewayRequest) params[0];

        logRequest(request);

        Object response = null;

        // call decorated method
        response = invocationContext.proceed();

        // if an exception is thrown here you need to check exception logs not
        // audit logs
        logResponse(request, response);

        return response;
    }

    private void logRequest(LoggableGatewayRequest request) {

        AuditTrailGateway message = new AuditTrailGateway(request.getGatewayName(), request.getRequestName(),
                request.toString(), AuditType.REQUEST);

        auditGatewaySender.auditApiGatewayRequest(message);

    }

    private void logResponse(LoggableGatewayRequest request, Object response) {

        String responseData = response == null ? null : response.toString();

        AuditTrailGateway message = new AuditTrailGateway(request.getGatewayName(), request.getRequestName(),
                responseData, AuditType.RESPONSE);

        auditGatewaySender.auditApiGatewayRequest(message);

    }

}

package com.wigroup.domain.audit;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

public class HideSensitiveDataEvent implements Serializable {
    
    private String uuid;
    private Object request;
    private Object response;
    private EventType event;
    private TimeUnit unit;
    private long frequency;
    
    public static final String LOGIN = "/rest/login";
    public static final String REGISTER_PIN = "/rest/user/register/pin";
    public static final String RESET_PIN = "/rest/login/reset";
    public static final String UPDATE_PIN = "/rest/pin/update";
    
    public HideSensitiveDataEvent(Object request, String uuid, EventType event, TimeUnit unit, long frequency) {
	this.request = request;
	this.uuid = uuid;
	this.event = event;
	this.unit = unit;
	this.frequency = frequency;
    }
    
    public HideSensitiveDataEvent(Object response, EventType event, String uuid, TimeUnit unit, long frequency) {
	this.response = response;
	this.uuid = uuid;
	this.event = event;
	this.unit = unit;
	this.frequency = frequency;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Object getRequest() {
        return request;
    }

    public void setRequest(Object request) {
        this.request = request;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    public EventType getEvent() {
        return event;
    }

    public void setEvent(EventType event) {
        this.event = event;
    }


    public TimeUnit getUnit() {
        return unit;
    }

    public void setUnit(TimeUnit unit) {
        this.unit = unit;
    }

    public long getFrequency() {
        return frequency;
    }

    public void setFrequency(long frequency) {
        this.frequency = frequency;
    }


    public enum EventType {
	
	ApiRequestResponse
        //Note: Other enums would be for external api trails 
    }
}

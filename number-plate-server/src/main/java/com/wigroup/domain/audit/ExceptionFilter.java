package com.wigroup.domain.audit;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;

@WebFilter(filterName = "ExceptionFilter")
public class ExceptionFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        logger.debug("Exception filter being applied");

        HttpRequestWrapper request = new HttpRequestWrapper((HttpServletRequest) servletRequest);
        HttpResponseWrapper response = new HttpResponseWrapper((HttpServletResponse) servletResponse);

        try {
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            //catches and logs any other uncaught exceptions to the database
            logger.error("Server error ", e);
            throw e;
        }

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.debug("Exception filter being created");
    }

    @Override
    public void destroy() {
    }

}

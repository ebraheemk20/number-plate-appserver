package com.wigroup.domain.audit;

import com.wigroup.application.constants.JNDINames;
import com.wigroup.domain.audit.entity.AuditTrailGateway;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;
import com.wigroup.infrastructure.persistence.CrudService;

@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "destination", propertyValue = JNDINames.AUDIT_API_GATEWAY_MESSAGE_QUEUE)
})
public class AuditApiGatewayMessageReceiver implements MessageListener {

    private final static Logger logger = LoggerFactory.getLogger(AuditApiGatewayMessageReceiver.class);

    @Inject
    private CrudService crudService;

    @Override
    public void onMessage(Message message) {

        try {
            AuditTrailGateway payload = message.getBody(AuditTrailGateway.class);
            if (!payload.isLogRequest()) {
                crudService.persist(payload);
            } else {
                AuditTrailGateway audit = crudService.findByIdAndColumnName(AuditTrailGateway.class, payload.getApi_uuid(), "api_uuid");
                if (audit != null) {
                    audit.setBody(payload.getBody());
                    crudService.merge(audit);
                }
            }
        } catch (JMSException e) {
            logger.error("Failed to log gateway call", e);
        }

    }

}

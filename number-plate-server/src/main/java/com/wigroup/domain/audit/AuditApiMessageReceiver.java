package com.wigroup.domain.audit;

import com.wigroup.application.constants.JNDINames;
import com.wigroup.domain.audit.constants.AuditType;
import com.wigroup.domain.audit.entity.AuditTrailApi;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;
import com.wigroup.infrastructure.persistence.CrudService;

@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "destination", propertyValue = JNDINames.AUDIT_API_MESSAGE_QUEUE)
})
public class AuditApiMessageReceiver implements MessageListener {

    private final static Logger logger = LoggerFactory.getLogger(AuditApiMessageReceiver.class);

    @Inject
    private CrudService crudService;

    @Override
    public void onMessage(Message message) {

        try {
            AuditTrailApi payload = message.getBody(AuditTrailApi.class);
            AuditType auditType = payload.getType();
            switch (auditType) {
                case REQUEST:
                    crudService.persist(payload);
                    break;
                case RESPONSE:
                    if (payload.getAudit_uuid() != null || !"".equals(payload.getAudit_uuid())) {
                        AuditTrailApi thisApiTrail = crudService.findByIdAndColumnName(AuditTrailApi.class,
                                payload.getAudit_uuid(), "audit_uuid");
                        thisApiTrail.setResponse_data(payload.getResponse_data());
                        thisApiTrail.setResponseStatus(payload.getResponseStatus());
                        crudService.persist(thisApiTrail);
                    }
                    break;
            }
        } catch (JMSException e) {
            logger.error("Failed to log API call", e);
        }

    }

}

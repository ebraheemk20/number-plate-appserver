package com.wigroup.domain.audit;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.wigroup.infrastructure.persistence.Auditable;

@SuppressWarnings("serial")
public class AuditTrailService extends Auditable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    protected AuditTrailService() {
    }

    @Override
    public Long getId() {

        return id;
    }

}

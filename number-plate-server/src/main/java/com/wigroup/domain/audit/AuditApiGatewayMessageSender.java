package com.wigroup.domain.audit;

import com.wigroup.application.constants.JNDINames;
import com.wigroup.domain.audit.entity.AuditTrailGateway;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Queue;

import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;

@Stateless
public class AuditApiGatewayMessageSender {

    private static final Logger logger = LoggerFactory.getLogger(AuditApiGatewayMessageSender.class);

    @Inject
    JMSContext context;

    @Resource(lookup = JNDINames.AUDIT_API_GATEWAY_MESSAGE_QUEUE)
    private Queue queue;

    public void auditApiGatewayRequest(AuditTrailGateway message) {

        if (logger.isDebugEnabled()) {
            logger.debug("Adding audit API gateway data to queue");
        }

        JMSProducer producer = context.createProducer();
        producer.send(queue, message);
    }

}

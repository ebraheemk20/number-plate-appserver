
package com.wigroup.domain.audit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wigroup.common.application.MessageDto;
import com.wigroup.common.exception.ApplicationException;
import com.wigroup.domain.audit.constants.AuditType;
import com.wigroup.domain.audit.entity.AuditTrailApi;
import com.wigroup.infrastructure.persistence.CrudService;
import com.wigroup.utils.ObjectMapperUtil;
import java.io.IOException;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.ejb.Timeout;

@Stateless
public class DataEncriptionService {
    
    @Resource
    private TimerService timerService;
    
    @Inject
    private AuditApiMessageSender auditApiMessageSender;
    
    @Inject
    private CrudService crudService;
    
    private static final ObjectMapper MAPPER = ObjectMapperUtil.getObjectMapper();
    
    
    //Note: This listens for events fired of this class type
    public void captureEvent(@Observes HideSensitiveDataEvent payload) {
	
	TimerConfig dataConfig = new TimerConfig(payload, false);
	timerService.createSingleActionTimer(payload.getUnit().toMillis(payload.getFrequency()), dataConfig);
    }
    
    @Timeout
    private void queueForProccessing(Timer timer){
        
        HideSensitiveDataEvent processData = (HideSensitiveDataEvent) timer.getInfo();
	HideSensitiveDataEvent.EventType event = processData.getEvent();
        
        switch (event) {
            case ApiRequestResponse:
                String path = (String)processData.getRequest();
                AuditTrailApi thisAuditTrailApi = getAuditTrail(processData.getUuid());
                
                switch(path){
//                    case(HideSensitiveDataEvent.REGISTER_PIN):
//                        
//                        CreatePinRequest registerPinRequest = (CreatePinRequest)fromJson(thisAuditTrailApi.getRequest_data(), CreatePinRequest.class);
//                        registerPinRequest.hidePins();
//                        String registerPinString = toJson(registerPinRequest);
//                        thisAuditTrailApi.setRequest_data(registerPinString);
//                        thisAuditTrailApi.setType(AuditType.RESPONSE);
//                        
//                        auditApiMessageSender.auditApiRequest(thisAuditTrailApi);
//                        
//                        break;
//                        
//                     case(HideSensitiveDataEvent.RESET_PIN):
//                         ResetPinRequest resetPinRequest = (ResetPinRequest)fromJson(thisAuditTrailApi.getRequest_data(), ResetPinRequest.class);
//                         resetPinRequest.hidePins();
//                         String resetPinString = toJson(resetPinRequest);
//                         thisAuditTrailApi.setRequest_data(resetPinString);
//                         thisAuditTrailApi.setType(AuditType.RESPONSE);
//                        
//                         auditApiMessageSender.auditApiRequest(thisAuditTrailApi);
//                        
//                         break;
//                      
//                     case(HideSensitiveDataEvent.LOGIN):
//                         LoginRequest loginRequest = (LoginRequest)fromJson(thisAuditTrailApi.getRequest_data(), LoginRequest.class);
//                         loginRequest.hidePin();
//                         String loginRequestString = toJson(loginRequest);
//                         thisAuditTrailApi.setRequest_data(loginRequestString);
//                         thisAuditTrailApi.setType(AuditType.RESPONSE);
//                         
//                         auditApiMessageSender.auditApiRequest(thisAuditTrailApi);
//            
//                         break;
//                         
//                     case(HideSensitiveDataEvent.UPDATE_PIN):
//                         UpdatePinRequest updatePinRequest = (UpdatePinRequest)fromJson(thisAuditTrailApi.getRequest_data(), UpdatePinRequest.class);
//                         updatePinRequest.hidePins();
//                         String updatePinRequestString = toJson(updatePinRequest);
//                         thisAuditTrailApi.setRequest_data(updatePinRequestString);
//                         thisAuditTrailApi.setType(AuditType.RESPONSE);
//                         
//                         auditApiMessageSender.auditApiRequest(thisAuditTrailApi);
//            
//                         break;    
//
              }
                
                break;
                
            //Note: Other external responses would be the other cases
        }
    }

    private AuditTrailApi getAuditTrail(String uuid) {
        
        AuditTrailApi thisAuditTrailApi = crudService.findByIdAndColumnName(AuditTrailApi.class, uuid, "audit_uuid");
        
        return thisAuditTrailApi;
    }
    
    private Object fromJson(String value, Class<?> type) {
	
	Object obj = null;
	try {
	     obj = MAPPER.readValue(value, type);
	} catch (IOException e) {
	    return null;
	}
	return obj;
    }
    
    private String toJson(Object obj) {
	
	String data = null;
	try {
	    data = MAPPER.writeValueAsString(obj);
	} catch (JsonProcessingException e) {
	    throw new ApplicationException(new MessageDto("-1","Error parsing json object to string"), e);
	}
	
	return data;
    }
    
}

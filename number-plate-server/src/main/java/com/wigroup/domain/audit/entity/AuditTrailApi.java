package com.wigroup.domain.audit.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.wigroup.domain.audit.constants.AuditType;
import com.wigroup.infrastructure.persistence.Auditable;

@SuppressWarnings("serial")
@Entity
@Table(name = "audit_trail_api")
public class AuditTrailApi extends Auditable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String audit_uuid;

    @Column(name = "processing_server_ip")
    private String serverIp;

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "api_id")
    private String platform;

    @Column(name = "os_version")
    private String osVersion;

    @Column(name = "http_method")
    private String method;

    private String url;

    @Column(columnDefinition = "text")
    private String header;

    @Column(columnDefinition = "text")
    private String request_data;

    @Column(columnDefinition = "text")
    private String response_data;

    @Column(name = "response_status")
    private Integer responseStatus;

    @Transient
    private AuditType type;

    protected AuditTrailApi() {
    }

    /**
     * For saving request data
     */
    public AuditTrailApi(String header, String deviceId, String platform, String osVersion, String method, String url,
            String requestBody, String serverIp, String audit_uuid) {

        this.serverIp = serverIp;
        this.deviceId = deviceId;
        this.method = method;
        this.url = url;
        this.platform = platform;
        this.osVersion = osVersion;
        this.header = header;
        this.request_data = requestBody;
        this.audit_uuid = audit_uuid;
        this.type = AuditType.REQUEST;
    }

    /**
     * For saving response data
     */
    public AuditTrailApi(Integer status, String deviceId, String platform, String osVersion, String method, String url, String responseBody, String audit_uuid) {

        // this allows us to see which URL was originally called and what we are
        // sending back as a result of the call
        this.url = url;
        this.deviceId = deviceId;
        this.method = method;
        this.platform = platform;
        this.osVersion = osVersion;
        this.response_data = responseBody;
        this.responseStatus = status;
        this.audit_uuid = audit_uuid;
        this.type = AuditType.RESPONSE;
    }

    public Long getId() {
        return id;
    }

    public String getServerIp() {
        return serverIp;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getPlatform() {
        return platform;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public String getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    public String getHeader() {
        return header;
    }

    public Integer getResponseStatus() {
        return responseStatus;
    }

    public AuditType getType() {
        return type;
    }

    @SuppressWarnings("unused")
    private void setId(Long id) {
        this.id = id;
    }

    @SuppressWarnings("unused")
    private void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    @SuppressWarnings("unused")
    private void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @SuppressWarnings("unused")
    private void setPlatform(String os) {
        this.platform = os;
    }

    @SuppressWarnings("unused")
    private void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    @SuppressWarnings("unused")
    private void setMethod(String method) {
        this.method = method;
    }

    @SuppressWarnings("unused")
    private void setUrl(String url) {
        this.url = url;
    }

    @SuppressWarnings("unused")
    private void setHeader(String header) {
        this.header = header;
    }

    @SuppressWarnings("unused")
	private void setAudit_uuid(String audit_uuid) {
        this.audit_uuid = audit_uuid;
    }

    public void setRequest_data(String request_data) {
        this.request_data = request_data;
    }

    public void setResponse_data(String response_data) {
        this.response_data = response_data;
    }

    public void setResponseStatus(Integer responseStatus) {
        this.responseStatus = responseStatus;
    }

    public void setType(AuditType type) {
        this.type = type;
    }

    public String getAudit_uuid() {
        return audit_uuid;
    }

    public String getRequest_data() {
        return request_data;
    }

    public String getResponse_data() {
        return response_data;
    }
}

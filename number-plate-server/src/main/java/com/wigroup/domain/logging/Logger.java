package com.wigroup.domain.logging;

public abstract class Logger {

    public abstract void error(String message);

    public abstract void error(Throwable cause);

    public abstract void error(String message, Throwable cause);

    public abstract void warn(String message);

    public abstract void info(String message);

    public abstract void debug(String message);

    public abstract void trace(String message);

    public abstract boolean isDebugEnabled();

    public abstract boolean isTraceEnabled();

    public abstract boolean isInfoEnabled();

}

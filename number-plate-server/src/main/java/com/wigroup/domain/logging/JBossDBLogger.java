package com.wigroup.domain.logging;

import com.wigroup.application.ApplicationContext;
import com.wigroup.domain.audit.entity.AuditTrailException;

public class JBossDBLogger extends Logger {

    private org.jboss.logging.Logger logger;

    protected JBossDBLogger(Class<?> clazz) {
        this.logger = org.jboss.logging.Logger.getLogger(clazz);
    }

    @Override
    public void error(String message) {
        this.logger.error(message);
        logErrorToDatabase(message, null);
    }

    @Override
    public void error(String message, Throwable cause) {
        this.logger.error(message, cause);
        logErrorToDatabase(message, cause);
    }

    @Override
    public void error(Throwable cause) {
        this.logger.error(cause);
        logErrorToDatabase(cause.getMessage(), cause);
    }

    private void logErrorToDatabase(String message, Throwable cause) {

        ExceptionLoggerSender exceptionLogger = ApplicationContext.getBean(ExceptionLoggerSender.class);

        AuditTrailException msg = new AuditTrailException(message, cause);
        exceptionLogger.logException(msg);

    }

    @Override
    public void warn(String message) {
        this.logger.warn(message);
    }

    @Override
    public void info(String message) {
        if (isInfoEnabled()) {
            this.logger.info(message);
        }
    }

    @Override
    public void debug(String message) {
        if (isDebugEnabled()) {
            this.logger.debug(message);
        }
    }

    @Override
    public void trace(String message) {
        if (isTraceEnabled()) {
            this.logger.trace(message);
        }
    }

    @Override
    public boolean isDebugEnabled() {
        return this.logger.isDebugEnabled();
    }

    @Override
    public boolean isTraceEnabled() {
        return this.logger.isTraceEnabled();
    }

    @Override
    public boolean isInfoEnabled() {
        return this.logger.isInfoEnabled();
    }

}

package com.wigroup.domain.logging;

public class LoggerFactory {

    public static Logger getLogger(Class<?> clazz) {
        return new JBossDBLogger(clazz);
    }
}

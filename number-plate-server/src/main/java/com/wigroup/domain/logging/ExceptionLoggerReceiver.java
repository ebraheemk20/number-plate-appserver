package com.wigroup.domain.logging;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.wigroup.domain.audit.entity.AuditTrailException;
import com.wigroup.infrastructure.persistence.CrudService;

@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "destination", propertyValue = ExceptionLoggerReceiver.MESSAGE_QUEUE)
})
public class ExceptionLoggerReceiver implements MessageListener {

    public static final String MESSAGE_QUEUE = "java:global/jms/exceptionLoggerMessageQueue";

    private final Logger logger = LoggerFactory.getLogger(ExceptionLoggerReceiver.class);

    @Inject
    private CrudService crudService;

    @Override
    public void onMessage(Message message) {

        try {
            AuditTrailException payload = message.getBody(AuditTrailException.class);
            crudService.persist(payload);
        } catch (JMSException e) {
            logger.error("Failed to log audit trail", e);
        }

    }

}

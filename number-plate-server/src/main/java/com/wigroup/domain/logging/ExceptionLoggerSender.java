package com.wigroup.domain.logging;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Queue;

import com.wigroup.domain.audit.entity.AuditTrailException;

@Stateless
public class ExceptionLoggerSender {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    JMSContext context;

    @Resource(lookup = ExceptionLoggerReceiver.MESSAGE_QUEUE)
    private Queue queue;

    public void logException(AuditTrailException message) {

        if (logger.isDebugEnabled()) {
            logger.debug("Adding exception to exception queue");
        }

        JMSProducer producer = context.createProducer();
        producer.send(queue, message);
    }

}

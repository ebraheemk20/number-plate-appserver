package com.wigroup.domain.admin.dto;

import java.io.Serializable;


public class EmailDto implements Serializable{
    
    private String emailAddress;
    private String emailTemplateId;

    public EmailDto() {
    }

    public EmailDto(String emailAddress, String emailTemplateId) {
        this.emailAddress = emailAddress;
        this.emailTemplateId = emailTemplateId;
    }

    public String getEmailTemplateId() {
        return emailTemplateId;
    }

    public void setEmailTemplateId(String emailTemplateId) {
        this.emailTemplateId = emailTemplateId;
    }
    
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
}

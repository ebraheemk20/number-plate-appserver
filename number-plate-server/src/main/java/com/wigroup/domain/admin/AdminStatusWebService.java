package com.wigroup.domain.admin;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wigroup.application.GenericResponse;


@Stateless
@Path("admin")
@Produces(MediaType.WILDCARD)
@Consumes(MediaType.WILDCARD)
public class AdminStatusWebService {

    @GET
    @Path("status/pulse")
    public Response getPulse() {

        return Response.ok(new GenericResponse("Server is running.")).build();
    }
}

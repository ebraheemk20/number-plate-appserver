
package com.wigroup.domain.admin;

import com.wigroup.domain.admin.dto.EmailDto;
import com.wigroup.application.api.ApiService;
import com.wigroup.common.rest.RestResponseBuilder;
import com.wigroup.domain.admin.helper.AdminMessageSender;
import com.wigroup.domain.message.MessageService;
import com.wigroup.domain.message.constants.MessageId;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("admin/email")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AdminEmailWebService {
    
    @Inject
    MessageService messageService;
    
    @Inject
    private ApiService apiService;
    
    @Inject
    AdminMessageSender adminMessageSender;
    
    //--------------------------------------------------------------------------
    // Send test email 
    //--------------------------------------------------------------------------
    @POST
    @Path("send")
    public Response sendEmail(@HeaderParam("apiKey") String apiKey, EmailDto email){

        apiService.validateAdminApiKey(apiKey);
        
        adminMessageSender.sendEmail(email);
        
        return RestResponseBuilder.buildResponse(messageService.getMessage(MessageId.LOGGED_IN));
    }
    
}

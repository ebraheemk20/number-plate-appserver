package com.wigroup.domain.admin;

public class AuditFilter {

    private String api;
    private String methodName;
    private Paging paging;

    public AuditFilter(String api, String methodName, Integer pageOffset, Integer pageSize) {

        this.api = api;
        this.methodName = methodName;
        this.paging = new Paging(pageOffset, pageSize);
    }

    public String getApi() {
        return api;
    }

    public Integer getPageOffset() {
        return paging.getPageOffset();
    }

    public Integer getPageSize() {
        return paging.getPageSize();
    }

    public String getMethodName() {
        return methodName;
    }

}

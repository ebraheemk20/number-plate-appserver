//
//package com.wigroup.domain.admin.helper;
//
//import co.wigroup.email.aws.Email;
//import co.wigroup.email.aws.EmailAwsHandler;
//import com.wigroup.application.constants.JNDINames;
//import com.wigroup.domain.admin.dto.EmailDto;
//import com.wigroup.domain.config.ConfigService;
//import com.wigroup.domain.config.constants.ConfigId;
//import com.wigroup.domain.email.constants.EmailTemplateId;
//import com.wigroup.domain.email.entity.EmailTemplate;
//import com.wigroup.domain.logging.Logger;
//import com.wigroup.domain.logging.LoggerFactory;
//import com.wigroup.domain.user.entity.EmailLog;
//import com.wigroup.infrastructure.persistence.CrudService;
//import java.text.MessageFormat;
//import javax.ejb.ActivationConfigProperty;
//import javax.ejb.MessageDriven;
//import javax.inject.Inject;
//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.MessageListener;
//
//
//@MessageDriven(activationConfig = {
//	@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
//	@ActivationConfigProperty(propertyName = "destination", propertyValue = JNDINames.ADMIN_MESSAGE_QUEUE) })
//public class AdminMessageReceiver implements MessageListener {
//    
//    private final static Logger logger = LoggerFactory.getLogger(AdminMessageReceiver.class);
//    
//    @Inject
//    ConfigService configService;
//    
//    @Inject
//    CrudService crudService;
//    
//    @Override
//    public void onMessage(Message message) {
//
//	try{
//            EmailDto email = message.getBody(EmailDto.class);
//            
//            if(email != null){
//                
//                EmailTemplateId templateId = null;
//                EmailTemplate emailTemplate = null;
//		String messageBody = null;
//		String emailAddress = null;
//		String emailSubject = "";
//                String segmentName = "Admin";
//               
//                emailTemplate = crudService.findById(EmailTemplate.class, 
//                        EmailTemplateId.valueOf(email.getEmailTemplateId()));      
//             
//                emailSubject = emailTemplate.getSubject();
//                messageBody = MessageFormat.format(emailTemplate.getBody(), segmentName);
//                emailAddress = email.getEmailAddress();
//                templateId = emailTemplate.getId();
//                
//                EmailLog emailLog = new EmailLog(emailAddress, emailSubject, messageBody, templateId);
//                crudService.persist(emailLog);
//                
//                EmailAwsHandler handler = new EmailAwsHandler(configService.getString(ConfigId.AWS_ACCESS_KEY),
//			configService.getString(ConfigId.AWS_SECRET_KEY),
//			configService.getString(ConfigId.EMAIL_FROM_ADDRESS));
//                
//                try{
//                    handler.send(new Email().addTo(emailAddress).withHtmlBody(messageBody).withSubject(emailSubject));
//                }catch(Exception e){
//                    logger.debug("Error sending email: " + e);
//                }
//            }
//            
//        } catch (JMSException ex) {
//            logger.error("Failed to send email ", ex);
//        }
//    }
//    
//}

package com.wigroup.domain.admin;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.wigroup.application.api.ApiService;
import com.wigroup.common.application.MessageDto;
import com.wigroup.common.exception.ApplicationException;
import com.wigroup.domain.message.entity.Message;
import com.wigroup.domain.message.MessageService;
import com.wigroup.domain.message.dto.MessageUpdateDto;
import com.wigroup.domain.message.MessagesWrapper;
import com.wigroup.infrastructure.persistence.CrudService;

@Stateless
@Path("admin/messages")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AdminMessageWebService {

    @Inject
    private CrudService crudService;

    @Inject
    private MessageService messageService;

    @Inject
    private ApiService apiService;

    @GET
    public Response getMessages(@HeaderParam("apiKey") String apiKey) {

        apiService.validateAdminApiKey(apiKey);

        List<Message> messages = crudService.findAll(Message.class);

        return Response.ok().entity(new MessagesWrapper(messages)).build();
    }

    @PUT
    public Response updateMessage(@HeaderParam("apiKey") String apiKey, MessageUpdateDto messageUpdateDto) {

        if (messageUpdateDto.getMessageId() == null ) {
            throw new ApplicationException("MessageId cannot be null", new MessageDto("-1", "MessageId cannot be null"));
        }

        apiService.validateAdminApiKey(apiKey);

        if (messageUpdateDto.getMessageId() != null) {
            Message id = crudService.findById(Message.class, messageUpdateDto.getMessageId());
            messageService.updateMessage(id.getId(), messageUpdateDto.getMessage(), messageUpdateDto.getHttpStatusCode());
        }

        return Response.status(Status.NO_CONTENT).build();
    }
}

//package com.wigroup.domain.admin;
//
//import javax.ejb.Stateless;
//import javax.inject.Inject;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.HeaderParam;
//import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//
//import com.wigroup.application.GenericResponse;
//import com.wigroup.application.api.ApiService;
//import com.wigroup.common.application.ClientMessage;
//import com.wigroup.domain.user.UserService;
//
//@Stateless
//@Path("admin/user")
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
//public class AdminUserWebService {
//
//    @Inject
//    private UserService userService;
//
//    @Inject
//    private ApiService apiService;
//
//    //--------------------------------------------------------------------------
//    // Disable an account - Admin
//    //--------------------------------------------------------------------------
//    @PUT
//    @Path("{id}")
//    public Response delete(@HeaderParam("apiKey") String apiKey, @PathParam("id") String mobile) {
//
//    	apiService.validateAdminApiKey(apiKey);
//
//        userService.suspendAccount(mobile);
//        return Response.ok(new GenericResponse(ClientMessage.ACCOUNT_DISABLED)).build();
//    }
//}

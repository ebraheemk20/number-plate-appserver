package com.wigroup.domain.admin;

public class Paging {

    private Integer pageOffset;
    private Integer pageSize;

    public Paging(Integer pageOffset, Integer pageSize) {
        // set defaults if null is passed thru.
        if (pageOffset == null || pageOffset < 0) {
            this.pageOffset = 0;
        } else {
            this.pageOffset = pageOffset;
        }

        if (pageSize == null || pageSize < 0 || pageSize.equals(0)) {
            this.pageSize = Integer.MAX_VALUE;
        } else {
            this.pageSize = pageSize;
        }
    }

    public Integer getPageOffset() {
        return pageOffset;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((pageOffset == null) ? 0 : pageOffset.hashCode());
        result = prime * result + ((pageSize == null) ? 0 : pageSize.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Paging other = (Paging) obj;
        if (pageOffset == null) {
            if (other.pageOffset != null) {
                return false;
            }
        } else if (!pageOffset.equals(other.pageOffset)) {
            return false;
        }
        if (pageSize == null) {
            if (other.pageSize != null) {
                return false;
            }
        } else if (!pageSize.equals(other.pageSize)) {
            return false;
        }
        return true;
    }

}

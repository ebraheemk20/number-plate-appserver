package com.wigroup.domain.admin;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;

import com.wigroup.application.api.ApiService;
import com.wigroup.common.application.MessageDto;
import com.wigroup.common.exception.ApplicationException;
import com.wigroup.domain.config.entity.Config;
import com.wigroup.domain.config.ConfigService;
import com.wigroup.domain.config.constants.ConfigId;
import com.wigroup.domain.config.dto.ConfigValueDto;
import com.wigroup.infrastructure.persistence.CrudService;

@Stateless
@Path("admin/config")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AdminConfigWebService {

    @Inject
    private CrudService crudService;

    @Inject
    private ConfigService configService;

    @Inject
    private ApiService apiService;

    @GET
    public Response getConfig(@HeaderParam("apiKey") String apiKey) {

        apiService.validateAdminApiKey(apiKey);

        List<Config> configs = crudService.findAll(Config.class);
        return Response.ok().entity(configs).build();
    }

    @PUT
    @Path("/{id}")
    public Response updateConfig(@HeaderParam("apiKey") String apiKey, @PathParam("id") ConfigId id, ConfigValueDto configValue) {

        if (StringUtils.isBlank(configValue.getValue())) {
            throw new ApplicationException("Config value cannot be null", new MessageDto("-1", "Config value cannot be null"));
        }

        apiService.validateAdminApiKey(apiKey);

        Config config = crudService.findById(Config.class, id);

        if (config == null) {
            throw new ApplicationException("Config not found", new MessageDto("-2", "Config not found"));
        }

        configService.updateConfig(config.getId(), configValue.getValue());
        return Response.ok().entity(config).build();
    }
}

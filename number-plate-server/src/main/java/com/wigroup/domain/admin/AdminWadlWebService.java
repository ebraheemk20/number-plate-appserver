package com.wigroup.domain.admin;

import java.io.IOException;
import java.io.InputStream;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;

import com.wigroup.application.GenericResponse;
import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;

//TODO: Add wadl for admin API

@Stateless
@Path("v1")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Consumes(MediaType.WILDCARD)
public class AdminWadlWebService {

    private static final Logger logger = LoggerFactory.getLogger(AdminWadlWebService.class);

    @Context
    private HttpServletRequest httpRequest;

    @GET
    public Response getMessages() throws IOException {

        InputStream instream = httpRequest.getServletContext().getResourceAsStream("/WEB-INF/Template_API.wadl");

        if (instream != null) {
            return Response.ok(IOUtils.toString(instream)).build();
        } else {
            return Response.status(Status.NOT_FOUND).entity(new GenericResponse("File not found")).build();
        }
    }
}

package com.wigroup.domain.admin;

import com.wigroup.application.GenericResponse;
import com.wigroup.application.api.ApiService;

import java.net.URI;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.wigroup.common.application.MessageDto;
import com.wigroup.common.exception.ApplicationException;
import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;
import com.wigroup.infrastructure.persistence.CrudService;
import com.wigroup.scheduledtasks.entity.ScheduledTask;
import com.wigroup.scheduledtasks.dto.ScheduledTaskDto;
import com.wigroup.scheduledtasks.entity.ScheduledTaskService;
import com.wigroup.scheduledtasks.service.ScheduledTaskWrapper;

@Stateless
@Path("admin/scheduledtasks")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AdminScheduledTaskWebService {

    @Inject
    private CrudService crudService;

    @Inject
    private ScheduledTaskService ScheduledTaskService;

    @Inject
    private ApiService apiService;

    private @HeaderParam("apiKey")
    String apiKey;

    private static final Logger LOG = LoggerFactory.getLogger(AdminScheduledTaskWebService.class);

    @GET
    public Response getTasks() {

        apiService.validateAdminApiKey(apiKey);
        List<ScheduledTask> tasks = crudService.findAllActive(ScheduledTask.class);

        return Response.ok().entity(new ScheduledTaskWrapper(tasks)).build();
    }

    @PUT
    @Path("/{id}")
    public Response updateTask(@PathParam("id") Integer id, ScheduledTaskDto scheduledTaskDto) {

    	apiService.validateAdminApiKey(apiKey);
        scheduledTaskDto = scheduledTaskDto.updateId(id);
        ScheduledTask scheduledTask = crudService.findById(ScheduledTask.class, scheduledTaskDto.getId());

        if (scheduledTask == null) {
            throw new ApplicationException("Scheduled Task not found", new MessageDto("-2", "Scheduled Task not found"));
        }

        scheduledTask = scheduledTask.updateScheduledTask(scheduledTaskDto);
        crudService.merge(scheduledTask);
        LOG.debug("done updating the scheduledTask");
        LOG.debug("resetting the scheduled task processor");
        ScheduledTaskService.init();
        return Response.ok().entity(new ScheduledTaskDto(scheduledTask)).build();
    }

    @POST
    @Path("/{id}/start")
    public Response start(@PathParam("id") Integer id, @Context UriInfo uriInfo) {

    	apiService.validateAdminApiKey(apiKey);

        ScheduledTask scheduledTask = crudService.findById(ScheduledTask.class, id);

        if (scheduledTask == null) {
            throw new ApplicationException("Scheduled Task not found", new MessageDto("-2", "Scheduled Task not found"));
        }

        ScheduledTaskService.executeTaskNow(id);
        return Response.created(URI.create(uriInfo.getRequestUri().toString() + "/" + id))
                .entity(new GenericResponse("Task started")).build();
    }
}

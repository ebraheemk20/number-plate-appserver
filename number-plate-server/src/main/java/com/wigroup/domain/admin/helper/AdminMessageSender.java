package com.wigroup.domain.admin.helper;


import com.wigroup.application.constants.JNDINames;
import com.wigroup.domain.admin.dto.EmailDto;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Queue;


@Stateless
public class AdminMessageSender {
    
    @Inject
    private JMSContext context;

    @Resource(lookup = JNDINames.ADMIN_MESSAGE_QUEUE)
    private Queue queue;

    public void sendEmail(EmailDto email) {

	JMSProducer producer = context.createProducer();
	EmailDto message = new EmailDto(email.getEmailAddress(), email.getEmailTemplateId());
	producer.send(queue, message);
    }
    
}

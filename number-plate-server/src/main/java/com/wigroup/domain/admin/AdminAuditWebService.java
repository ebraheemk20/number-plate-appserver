package com.wigroup.domain.admin;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wigroup.application.api.ApiService;
import com.wigroup.domain.audit.AuditRepository;
import com.wigroup.domain.audit.entity.AuditTrailApi;
import com.wigroup.domain.audit.entity.AuditTrailException;
import com.wigroup.domain.audit.entity.AuditTrailGateway;
import com.wigroup.domain.audit.AuditTrailService;
import com.wigroup.domain.config.ConfigService;
import com.wigroup.domain.config.constants.ConfigId;
import com.wigroup.infrastructure.persistence.CrudService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Stateless
@Path("admin/audit")
@Api(value = "admin/audit")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AdminAuditWebService {

    @Inject
    private AuditRepository auditRepository;

    @Inject
    private ConfigService configService;

    @Inject
    private CrudService crudService;

    private @HeaderParam("apiKey")
    String apiKey;

    @Inject
    private ApiService apiService;

    @GET
    @Path("/api")
    @ApiOperation(httpMethod = HttpMethod.GET, value = "Get API audit trail entries", notes = "Returns audit trail API entries and their respective gateway audit trail entries.", response = AuditTrailApi.class)
    public Response getAuditTrailApi(
            @ApiParam(value = "Filter audit trail results by this Api ID", required = false) @QueryParam("apiIdFilter") String osFilter,
            @ApiParam(value = "Filter audit trail results by this method name", required = false) @QueryParam("methodNameFilter") String methodNameFilter,
            @ApiParam(value = "The number of result pages to skip", defaultValue = "0", required = false) @QueryParam("pageOffset") Integer pageOffset,
            @ApiParam(value = "The number of result entries returned", defaultValue = "10", required = false) @QueryParam("pageSize") Integer pageSize) {

        apiService.validateAdminApiKey(apiKey);
        if (pageSize == null) {
            pageSize = configService.getInt(ConfigId.DEFAULT_PAGE_SIZE);
        }

        AuditFilter filter = new AuditFilter(osFilter, methodNameFilter, pageOffset, pageSize);

        List<AuditTrailApi> results = auditRepository.findAuditTrailApi(filter);
        return Response.ok().entity(results).build();

    }

    @GET
    @Path("/api/{id}")
    @ApiOperation(httpMethod = HttpMethod.GET, value = "Get a specific API audit trail entry", notes = "Returns a specific audit trail API entry.", response = AuditTrailApi.class)
    public Response getSpecificAuditTrailApi(
            @ApiParam(value = "The ID of the entry", required = true) @PathParam("id") Long id) {

        apiService.validateAdminApiKey(apiKey);
        AuditTrailApi api = crudService.findById(AuditTrailApi.class, id);
        return Response.ok().entity(api).build();

    }

    @GET
    @Path("/exception")
    @ApiOperation(httpMethod = HttpMethod.GET, value = "Get exception audit trail entries", notes = "Returns audit trail exception entries.", response = AuditTrailException.class)
    public Response getAuditTrailException(
            @ApiParam(value = "The number of result pages to skip", defaultValue = "0", required = false) @QueryParam("pageOffset") Integer pageOffset,
            @ApiParam(value = "The number of result entries returned", defaultValue = "3", required = false) @QueryParam("pageSize") Integer pageSize) {

        Paging paging = new Paging(pageOffset, pageSize);
        apiService.validateAdminApiKey(apiKey);
        List<AuditTrailException> results = crudService.findAll(AuditTrailException.class, paging);
        return Response.ok().entity(results).build();
    }

    @GET
    @Path("/exception/{id}")
    @ApiOperation(httpMethod = HttpMethod.GET, value = "Get a specific exception audit trail entry", notes = "Returns an audit trail exception entry.", response = AuditTrailException.class)
    public Response getSpecificAuditTrailException(
            @ApiParam(value = "The ID of the entry", required = true) @PathParam("id") Long id) {

        apiService.validateAdminApiKey(apiKey);
        AuditTrailException api = crudService.findById(AuditTrailException.class, id);
        return Response.ok().entity(api).build();
    }

    @GET
    @Path("/service")
    @ApiOperation(httpMethod = HttpMethod.GET, value = "Get service audit trail entries", notes = "Returns audit trail service entries and their respective gateway audit trail entries.", response = AuditTrailService.class)
    public Response getAuditTrailService(
            @ApiParam(value = "The number of result pages to skip", defaultValue = "0", required = false) @QueryParam("pageOffset") Integer pageOffset,
            @ApiParam(value = "The number of result entries returned", defaultValue = "10", required = false) @QueryParam("pageSize") Integer pageSize) {

        Paging paging = new Paging(pageOffset, pageSize);
        apiService.validateAdminApiKey(apiKey);
        List<AuditTrailGateway> results = crudService.findAll(AuditTrailGateway.class, paging);
        return Response.ok().entity(results).build();

    }

    @GET
    @Path("/service/{id}")
    @ApiOperation(httpMethod = HttpMethod.GET, value = "Get specific service audit trail entry", notes = "Returns  specific audit trail service entry.", response = AuditTrailService.class)
    public Response getSpecificAuditTrailService(
            @ApiParam(value = "The ID of the entry", required = true) @PathParam("id") Long id) {

        apiService.validateAdminApiKey(apiKey);
        AuditTrailGateway api = crudService.findById(AuditTrailGateway.class, id);
        return Response.ok().entity(api).build();
    }
}

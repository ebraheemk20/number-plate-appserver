package com.wigroup.infrastructure.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class LocalDateConverter implements AttributeConverter<LocalDateTime, Timestamp> {

    @Override
    public Timestamp convertToDatabaseColumn(LocalDateTime attribute) {

	if (attribute != null) {
	    Instant instant = Instant.from(attribute.atZone(ZoneId.systemDefault()));
	    return Timestamp.from(instant);
	}

	return null;
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Timestamp dbData) {

	if (dbData != null) {
	    
	    return LocalDateTime.parse(dbData.toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S"));
	}
	
	return null;
    }
}

package com.wigroup.infrastructure.persistence;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.wigroup.application.HasStatus;
import com.wigroup.common.persistence.Persistable;
import com.wigroup.domain.admin.Paging;

@Stateless
public class CrudService {

    @PersistenceContext
    private EntityManager entityManager;

    public <T extends Persistable> T persist(T entity) {
        entityManager.persist(entity);
        entityManager.flush();
        entityManager.refresh(entity);
        return entity;
    }

    public <T extends Persistable> T merge(T entity) {
        return (T) entityManager.merge(entity);
    }

    public void delete(Persistable entity) {
        delete(entity.getClass(), entity.getId());
    }

    public void delete(Class<? extends Persistable> clazz, Object id) {
        Object entity = entityManager.getReference(clazz, id);
        entityManager.remove(entity);
    }

    public <T> T refresh(T entity) {
        entityManager.refresh(entity);
        return entity;
    }

    public <T extends Persistable, ID> T findById(Class<T> type, ID id) {

        TypedQuery<T> query = entityManager.createQuery(" from " + type.getSimpleName() + " where id = :id", type);
        query.setParameter("id", id);

        List<T> results = query.getResultList();

        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    public <T extends Persistable, ID> T findByIdAndColumnName(Class<T> type, ID id, String column) {

        TypedQuery<T> query = entityManager.createQuery(" from " + type.getSimpleName() + " where " + column + " = :id", type);
        query.setParameter("id", id);

        List<T> results = query.getResultList();

        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    public <T extends Persistable, ID> T findByColumnName(Class<T> type, String column, String value) {

        TypedQuery<T> query = entityManager.createQuery(" from " + type.getSimpleName() + " where " + column + " = :id", type);
        query.setParameter("id", value);

        List<T> results = query.getResultList();

        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    public <T extends Persistable> List<T> findAll(Class<T> type) {
        TypedQuery<T> query = entityManager.createQuery(" from " + type.getSimpleName(), type);
        return query.getResultList();
    }

    public <T extends Persistable> List<T> findAll(Class<T> type, Paging paging) {
        TypedQuery<T> query = entityManager.createQuery(" from " + type.getSimpleName(), type);
        query.setFirstResult(paging.getPageOffset() * paging.getPageSize());
        query.setMaxResults(paging.getPageSize());
        return query.getResultList();
    }

    public <T extends HasStatus> List<T> findAllActive(Class<T> type) {
        TypedQuery<T> query = entityManager.createQuery(" select a from " + type.getSimpleName() + " a where status = 'ACTIVE' ", type);
        return query.getResultList();
    }

}

package com.wigroup.infrastructure.persistence;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.wigroup.common.persistence.Persistable;
import java.time.LocalDateTime;

/**
 * Interface for all JPA Entities.
 */
@MappedSuperclass
public abstract class Auditable implements Persistable {

    @Column(name = "date_created", updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    protected LocalDateTime dateCreated;

    @Column(name = "last_updated", updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    protected LocalDateTime lastUpdated;

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

}

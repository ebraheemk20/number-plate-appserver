package com.wigroup.infrastructure;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.inject.Inject;

import com.wigroup.domain.audit.AuditApiGateway;
import com.wigroup.domain.config.ConfigService;
import com.wigroup.domain.config.constants.ConfigId;
import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;
import com.wigroup.sms.client.SmsClient;
import com.wigroup.sms.client.request.SendSmsRequest;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@DependsOn("ConfigService")
@Startup
public class SmsService {

    private static final Logger logger = LoggerFactory.getLogger(SmsService.class);

    @Inject
    private ConfigService configService;

    private SmsClient smsClient;

    @PostConstruct
    public void init() {

        final String SMS_GATEWAY_IP = configService.getString(ConfigId.SMS_GATEWAY_IP);
        final Integer SMS_GATEWAY_PORT = configService.getInt(ConfigId.SMS_GATEWAY_PORT);
        final String SMS_GATEWAY_CLIENT_ID = configService.getString(ConfigId.SMS_GATEWAY_CLIENT_ID);
        final String SMS_GATEWAY_CLIENT_PASSWORD = configService.getString(ConfigId.SMS_GATEWAY_CLIENT_PASSWORD);
        final Boolean SMS_GATEWAY_CLIENT_IS_SSL = configService.getBoolean(ConfigId.SMS_GATEWAY_CLIENT_IS_SSL);
        final Integer SMS_GATEWAY_TIMEOUT_MS = configService.getInt(ConfigId.SMS_GATEWAY_TIMEOUT_MS);

        smsClient = new SmsClient(SMS_GATEWAY_IP, SMS_GATEWAY_PORT, SMS_GATEWAY_CLIENT_ID, SMS_GATEWAY_CLIENT_PASSWORD, SMS_GATEWAY_CLIENT_IS_SSL, SMS_GATEWAY_TIMEOUT_MS);

    }

    @AuditApiGateway
    public void sendSms(SendSmsRequest request) {

        try {
            smsClient.sendSms(request);
        } catch (Exception e) {
            logger.error("Error sending SMS: ", e);
        }

    }

}

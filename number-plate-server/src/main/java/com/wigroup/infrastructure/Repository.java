package com.wigroup.infrastructure;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class Repository {

    @PersistenceContext
    protected EntityManager entityManager;

}

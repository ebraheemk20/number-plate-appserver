//package com.wigroup.interfaces;
//
//import javax.ejb.Stateless;
//import javax.inject.Inject;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.HeaderParam;
//import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//
//import com.wigroup.application.GenericResponse;
//import com.wigroup.application.api.ApiService;
//import com.wigroup.common.application.ClientMessage;
//import com.wigroup.common.exception.ApplicationException;
//import com.wigroup.common.rest.RestResponseBuilder;
//import com.wigroup.domain.config.ConfigService;
//import com.wigroup.domain.config.constants.ConfigId;
//import com.wigroup.domain.message.MessageService;
//import com.wigroup.domain.message.constants.MessageId;
//import com.wigroup.domain.otp.OtpService;
//import com.wigroup.domain.pin.PinService;
//import com.wigroup.domain.user.entity.User;
//import com.wigroup.domain.user.UserService;
//import com.wigroup.domain.user.dto.Communication;
//import com.wigroup.domain.user.dto.LoginRequest;
//import com.wigroup.domain.user.dto.ResetPinRequest;
//import com.wigroup.security.JWTUtils;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import io.swagger.annotations.ResponseHeader;
//
///**
// * Not really a login, but more an authenticate user service.
// */
//@Stateless
//@Path("login")
//@Api(value = "login")
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
//public class LoginWebService {
//
//    @Inject
//    private UserService userService;
//
//    @Inject
//    private ConfigService configService;
//
//    @Inject
//    private ApiService apiService;
//
//    @Inject
//    private MessageService messageService;
//    
//    @Inject
//    private PinService pinService;
//    
//    @Inject
//    private OtpService otpService;
//    
//    //--------------------------------------------------------------------------
//    // User login / authentication
//    //--------------------------------------------------------------------------
//    @ApiOperation(value = "User login / authentication", notes = "Logs in, authenticates user")
//    @ApiResponses(value = { 
//    	      @ApiResponse(code = 200, message = "Logged in", 
//    	                   responseHeaders = @ResponseHeader(name = "Authorization-X", description = "JWT Token string", response = String.class)),
//    	      @ApiResponse(code = 401, message = "Missing api key"),
//    	      @ApiResponse(code = 400, message = "Missing body")})
//    @POST
//    public Response login(@ApiParam (required = true) @HeaderParam("apiKey") String apiKey,
//    					  @ApiParam (required = true, name = "LoginRequest") LoginRequest request) {
//
//        if (apiKey == null) {
//            throw new ApplicationException("Missing api key", messageService.getMessage(ClientMessage.MISSING_API_KEY));
//        }
//        
//        if (request == null){
//            throw new ApplicationException("Missing body", messageService.getMessage(ClientMessage.BODY_MISSING));
//        }
//
//        apiService.validateApiKey(apiKey);
//        User user = userService.authenticateUser(request);
//
//        Integer timeout = configService.getInt(ConfigId.SESSION_TIMEOUT_SECONDS);
//        return Response.ok().header("Authorization-X", JWTUtils.sign(timeout, user.getMobileNumber()))
//                .entity(new GenericResponse(messageService.getMessage(MessageId.LOGGED_IN))).build();
//    }
//
//    
//    //--------------------------------------------------------------------------
//    // User forgot PIN - They are not logged in. Send otp
//    //--------------------------------------------------------------------------
//    @ApiOperation(value = "User forgot PIN - They are not logged in. Send otp", notes = "User forgot PIN and user is not logged in. App Sends otp to confirm")
//    @ApiResponses(value = { 
//    	      @ApiResponse(code = 200, message = "OTP Sent"),
//    	      @ApiResponse(code = 401, message = "Missing api key"),
//    	      @ApiResponse(code = 400, message = "Missing body")})
//    @POST
//    @Path("forgot")
//    public Response forgotPin(@ApiParam (required = true) @HeaderParam("apiKey") String apiKey, 
//    						  @ApiParam (required = true, name = "ForgotPin") Communication forgotPin) {
//
//        if (apiKey == null) {
//            throw new ApplicationException("Missing api key", messageService.getMessage(ClientMessage.MISSING_API_KEY));
//        }
//        
//        if(forgotPin == null){
//            throw new ApplicationException("Missing body", messageService.getMessage(ClientMessage.BODY_MISSING));
//        }
//
//        apiService.validateApiKey(apiKey);
//        otpService.sendOtp(forgotPin.getMobile(), true, null);
//
//        return RestResponseBuilder.buildResponse(messageService.getMessage(ClientMessage.OTP_SENT));
//    }
//    
//    //--------------------------------------------------------------------------
//    // Reset users PIN - They are not logged in. Forgot their PIN
//    //--------------------------------------------------------------------------
//    
//    @ApiOperation(value = "Reset users PIN", notes = "Reset users PIN - They are not logged in. Forgot their PIN")
//    @ApiResponses(value = { 
//    	      @ApiResponse(code = 200, message = "Pin updated"),
//    	      @ApiResponse(code = 401, message = "Missing api key"),
//    	      @ApiResponse(code = 400, message = "Missing body")})
//    @PUT
//    @Path("reset")
//    public Response resetPin(@ApiParam (required = true) @HeaderParam("apiKey") String apiKey, 
//    						 @ApiParam (required = true, name = "ResetPinRequest") ResetPinRequest request) {
//
//        if (apiKey == null) {
//            throw new ApplicationException("Missing api key", messageService.getMessage(ClientMessage.MISSING_API_KEY));
//        }
//        
//        if (request == null) {
//            throw new ApplicationException("Missing body", messageService.getMessage(ClientMessage.BODY_MISSING));
//        }
//        apiService.validateApiKey(apiKey);
//
//        String mobile = request.getMobile();
//        pinService.resetPin(mobile, true, request);
//       
//        return RestResponseBuilder.buildResponse(messageService.getMessage(ClientMessage.PIN_UPDATED));
//    }
//}

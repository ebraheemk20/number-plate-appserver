//package com.wigroup.interfaces;
//
//import com.wigroup.application.GenericResponse;
//import javax.ejb.Stateless;
//import javax.inject.Inject;
//import javax.servlet.http.HttpServletRequest;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.GET;
//import javax.ws.rs.HeaderParam;
//import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.Context;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//
//import com.wigroup.application.api.ApiService;
//import com.wigroup.common.application.ClientMessage;
//import com.wigroup.common.application.MessageDto;
//import com.wigroup.common.exception.ApplicationException;
//import com.wigroup.common.rest.RestResponseBuilder;
//import com.wigroup.domain.config.ConfigService;
//import com.wigroup.domain.config.constants.ConfigId;
//import com.wigroup.domain.logging.Logger;
//import com.wigroup.domain.logging.LoggerFactory;
//import com.wigroup.domain.message.MessageService;
//import com.wigroup.domain.otp.OtpService;
//import com.wigroup.domain.pin.PinService;
//import com.wigroup.domain.user.UserService;
//import com.wigroup.domain.user.dto.Communication;
//import com.wigroup.domain.user.dto.CreatePinRequest;
//import com.wigroup.domain.user.dto.RegisterUserRequest;
//import com.wigroup.domain.user.dto.UpdateUserRequest;
//import com.wigroup.domain.user.dto.UserProfileResponse;
//import com.wigroup.domain.user.dto.ValidateOtpRequest;
//import com.wigroup.security.JWTUtils;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import io.swagger.annotations.ResponseHeader;
//
//@Stateless
//@Path("user")
//@Api(value = "user")
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
//public class UserWebService {
//
//    private static final Logger logger = LoggerFactory.getLogger(UserWebService.class);
//
//    @Inject
//    private UserService userService;
//
//    @Inject
//    private ConfigService configService;
//
//    @Inject
//    private ApiService apiService;
//
//    @Context
//    private HttpServletRequest httpRequest;
//
//    @Inject
//    private MessageService messageService;
//    
//    @Inject
//    private PinService pinService;
//    
//    @Inject
//    private OtpService otpService;
//    
//    //--------------------------------------------------------------------------
//    // Register user endpoint
//    //--------------------------------------------------------------------------
//    @ApiOperation(value = "Register User", notes = "Registers a new user and sends OTP for verification")
//    @ApiResponses(value = { 
//    	      @ApiResponse(code = 201, message = "User registered pending verification"),
//    	      @ApiResponse(code = 401, message = "Missing api key"),
//    	      @ApiResponse(code = 400, message = "Missing body")})
//    @POST
//    @Path("register")
//    public Response register(@ApiParam (required = true) @HeaderParam("apiKey") String apiKey, 
//    						 @ApiParam (required = true, name = "RegisterUserRequest") RegisterUserRequest registerUserRequest) {
//
//        if (apiKey == null) {
//            throw new ApplicationException("Missing api key",
//                    messageService.getMessage(ClientMessage.MISSING_API_KEY));
//        }
//        
//        if(registerUserRequest == null){
//            throw new ApplicationException("Missing body", messageService.getMessage(ClientMessage.BODY_MISSING));
//        }
//
//        apiService.validateApiKey(apiKey);
//        userService.registerUser(registerUserRequest);
//
//        return RestResponseBuilder.buildResponse(messageService.getMessage(ClientMessage.USER_REGISTERED_PENDING_VERIFICATION));
//    }
//
//    //--------------------------------------------------------------------------
//    // Validate registration otp endpoint
//    //--------------------------------------------------------------------------
//    @ApiOperation(value = "Validate registration OTP", notes = "Validates OTP sent for registration")
//    @ApiResponses(value = { 
//    	      @ApiResponse(code = 200, message = "OTP Validated"),
//    	      @ApiResponse(code = 401, message = "Missing api key"),
//    	      @ApiResponse(code = 400, message = "Missing body")})
//    @POST
//    @Path("register/otp/validate")
//    public Response validateMobileNumber(@ApiParam (required = true) @HeaderParam("apiKey") String apiKey, 
//    									 @ApiParam (required = true, name = "ValidateOtpRequest") ValidateOtpRequest request) {
//
//        if (apiKey == null) {
//            throw new ApplicationException("Missing api key",
//                    messageService.getMessage(ClientMessage.MISSING_API_KEY));
//        }
//        
//        if(request == null){
//            throw new ApplicationException("Missing body", messageService.getMessage(ClientMessage.BODY_MISSING));
//        }
//
//        apiService.validateApiKey(apiKey);
//        otpService.validateRegistrationOtp(request);
//
//        return RestResponseBuilder.buildResponse(messageService.getMessage(ClientMessage.OTP_VALIDATED));
//    }
//
//    //--------------------------------------------------------------------------
//    // Resend otp endpoint
//    //--------------------------------------------------------------------------
//    @ApiOperation(value = "Resend OTP", notes = "Resends OTP for registration verification")
//    @ApiResponses(value = { 
//    	      @ApiResponse(code = 201, message = "OTP Sent"),
//    	      @ApiResponse(code = 401, message = "Missing api key"),
//    	      @ApiResponse(code = 400, message = "Missing body")})
//    @POST
//    @Path("register/otp")
//    public Response resendOtp(@ApiParam (required = true) @HeaderParam("apiKey") String apiKey, 
//    						  @ApiParam (required = true, name = "ResetOtpRequest") Communication request) {
//
//        if (apiKey == null) {
//            throw new ApplicationException("Missing api key",
//                    messageService.getMessage(ClientMessage.MISSING_API_KEY));
//        }
//        
//        if (request == null){
//            throw new ApplicationException("Missing body", ClientMessage.BODY_MISSING);
//        }
//
//        apiService.validateApiKey(apiKey);
//        otpService.sendOtp(request.getMobile(), true, null);
//
//        return RestResponseBuilder.buildResponse(messageService.getMessage(ClientMessage.OTP_SENT));
//    }
//    
//    //--------------------------------------------------------------------------
//    // Create pin endpoint
//    //--------------------------------------------------------------------------
//    @ApiOperation(value = "Create Pin", notes = "Create a PIN from user's request")
//    @ApiResponses(value = { 
//    	      @ApiResponse(code = 201, message = "PIN Created",
//    	    		  responseHeaders = @ResponseHeader(name = "Authorization-X", description = "JWT Token string", response = String.class)),
//    	      @ApiResponse(code = 401, message = "Missing api key"),
//    	      @ApiResponse(code = 400, message = "Missing body")})
//    @POST
//    @Path("register/pin")
//    public Response createPin(@ApiParam (required = true) @HeaderParam("apiKey") String apiKey, 
//    						  @ApiParam (required = true, name = "CreatePinRequest") CreatePinRequest request) {
//
//	if (apiKey == null) {
//	    throw new ApplicationException("Missing api key", messageService.getMessage(ClientMessage.MISSING_API_KEY));
//	}
//
//	if (request == null) {
//	    throw new ApplicationException("Missing request object",
//		    messageService.getMessage(ClientMessage.BODY_MISSING));
//	}
//
//	apiService.validateApiKey(apiKey);
//        pinService.createPin(request);
//	Integer timeout = configService.getInt(ConfigId.SESSION_TIMEOUT_SECONDS);
//	MessageDto message = messageService.getMessage(ClientMessage.PIN_UPDATED);
//
//	return Response.status(message.getHttpStatusCode())
//		.header("Authorization-X", JWTUtils.sign(timeout, request.getMobile())).entity(new GenericResponse(message)).build();
//    }
//    
//    //--------------------------------------------------------------------------
//    // Update user details endpoint
//    //--------------------------------------------------------------------------
//    @ApiOperation(value = "Update user details / profile", notes = "Update user details / profile")
//    @ApiResponses(value = { 
//    	      @ApiResponse(code = 201, message = "User updated"),
//    	      @ApiResponse(code = 401, message = "Missing api key")})
//    @PUT
//    @Path("profile")
//    public Response update(@ApiParam (required = true) @HeaderParam("Authorization-X") String authorization_X, 
//    					   @ApiParam (required = true, name = "UpdateUserRequest") UpdateUserRequest updateUserRequest) {
//        
//        if (updateUserRequest == null){
//            throw new ApplicationException("Missing body", ClientMessage.BODY_MISSING);
//        }
//
//        String mobile = (String) httpRequest.getAttribute("identifier");
//        userService.updateUser(updateUserRequest, mobile);
//
//        return RestResponseBuilder.buildResponse(messageService.getMessage(ClientMessage.USER_UPDATED));
//    }
//
//    //--------------------------------------------------------------------------
//    // Get user profile endpoint
//    //--------------------------------------------------------------------------
//    @ApiOperation(value = "Get user details / profile", notes = "Gets the user details / profile")
//    @ApiResponses(value = { 
//    	      @ApiResponse(code = 200, message = "OK", response = UserProfileResponse.class)})
//    @GET
//    @Path("profile")
//    public UserProfileResponse getProfile(@ApiParam (required = true) @HeaderParam("Authorization-X") String authorization_X) {
//
//        String mobile = (String) httpRequest.getAttribute("identifier");
//
//        UserProfileResponse response = userService.getProfile(mobile);
//        return response;
//    }
//}

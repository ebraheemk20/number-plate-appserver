package com.wigroup.scheduledtasks;

import com.wigroup.application.ApplicationContext;
import com.wigroup.domain.audit.AuditRepository;

public class ClearLogsTask implements Runnable {

    @Override
    public void run() {

        AuditRepository repo = ApplicationContext.getBean(AuditRepository.class);
        repo.clearLogs();

    }

}

package com.wigroup.scheduledtasks.entity;

import com.wigroup.scheduledtasks.dto.ScheduledTaskDto;
import java.util.concurrent.TimeUnit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.wigroup.application.HasStatus;
import com.wigroup.common.Status;
import com.wigroup.infrastructure.persistence.Auditable;

@Entity
@Table(name = "scheduled_task")
public class ScheduledTask extends Auditable implements HasStatus {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private Long frequency;

    @Enumerated(EnumType.STRING)
    @Column(name = "time_unit")
    private TimeUnit timeUnit;

    @Enumerated(EnumType.STRING)
    private Status status;

    protected ScheduledTask() {
    }

    @Override
    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Status getStatus() {
        return status;
    }

    @SuppressWarnings("unused")
    private void setId(Integer id) {
        this.id = id;
    }

    @SuppressWarnings("unused")
    private void setName(String name) {
        this.name = name;
    }

    @SuppressWarnings("unused")
    private void setStatus(Status status) {
        this.status = status;
    }

    public Long getFrequency() {
        return frequency;
    }

    @SuppressWarnings("unused")
    private void setFrequency(Long frequency) {
        this.frequency = frequency;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    @SuppressWarnings("unused")
    private void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    public ScheduledTask updateScheduledTask(ScheduledTaskDto scheduledTaskDto) {
        if (scheduledTaskDto.getFrequency() != null || scheduledTaskDto.getFrequency() != 0) {
            this.frequency = scheduledTaskDto.getFrequency();
        }
        if (scheduledTaskDto.getTimeUnit() != null) {
            this.timeUnit = scheduledTaskDto.getTimeUnit();
        }
        if (scheduledTaskDto.getStatus() != null) {
            this.status = scheduledTaskDto.getStatus();
        }
        return this;
    }

}

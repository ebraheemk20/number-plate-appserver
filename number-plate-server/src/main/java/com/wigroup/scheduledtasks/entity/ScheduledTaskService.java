package com.wigroup.scheduledtasks.entity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import javax.inject.Inject;

import com.wigroup.common.Status;
import com.wigroup.domain.logging.Logger;
import com.wigroup.domain.logging.LoggerFactory;
import com.wigroup.infrastructure.persistence.CrudService;
import com.wigroup.scheduledtasks.constants.Task;

@Singleton
@Lock(LockType.READ)
@Startup
@DependsOn("CrudService")
public class ScheduledTaskService {

    private static Logger logger = LoggerFactory.getLogger(ScheduledTaskService.class);

    @Resource
    private ManagedScheduledExecutorService scheduledExecutor;

    @Resource
    private ManagedExecutorService excecutorService;

    @Inject
    private CrudService crudService;

    private Map<String, Task> tasksMap = new HashMap<>();

    @PostConstruct
    public void init() {

        for (Task task : Task.values()) {

            ScheduledTask scheduledTask = crudService.findByColumnName(ScheduledTask.class, "name", task.toString());

            if (scheduledTask.getStatus() == Status.ACTIVE) {

                try {

                    Runnable taskClass = (Runnable) task.getClazz().newInstance();

                    TimeUnit timeUnit = scheduledTask.getTimeUnit();
                    Long frequency = scheduledTask.getFrequency();
                    Long initialDelay = 0L;

//					if (timeUnit == TimeUnit.DAYS && frequency.equals(1L) ||
//							timeUnit == TimeUnit.HOURS && frequency.equals(24L)) {
//						// run it at night time
//						LocalDate date = LocalDate.now();
//						LocalDateTime dateMidnight = LocalDate.now().plusDays(1).atStartOfDay();
//						long numberOfHours = Duration.between(date, dateMidnight).toHours();
//
//						// set unit and frequency to hours
//						timeUnit = TimeUnit.HOURS;
//						frequency = 24L;
//						initialDelay = numberOfHours;
//
//					}
                    scheduledExecutor.scheduleAtFixedRate(taskClass, initialDelay, frequency, timeUnit);

                } catch (InstantiationException | IllegalAccessException e) {
                    logger.error("Unable to instantiate scheduled task class", e);
                }
            }
        }
    }

    public void executeTaskNow(Integer id) {

        ScheduledTask scheduledTask = crudService.findById(ScheduledTask.class, id);
        if (scheduledTask.getStatus() == Status.ACTIVE) {
            tasksMap = Arrays.stream(Task.values()).collect(Collectors.toMap(Task::toString, task -> task));

            Task task = tasksMap.get(scheduledTask.getName());
            Runnable taskClass;
            try {
                taskClass = (Runnable) task.getClazz().newInstance();
                excecutorService.execute(taskClass);
            } catch (InstantiationException | IllegalAccessException e) {
                logger.error("Unable to instantiate scheduled task class", e);
            }
        }
    }
}

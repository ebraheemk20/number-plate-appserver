package com.wigroup.scheduledtasks.constants;

public enum Unit {
    MINUTES, HOURS;
}

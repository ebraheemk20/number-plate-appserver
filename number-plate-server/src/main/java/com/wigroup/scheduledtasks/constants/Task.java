package com.wigroup.scheduledtasks.constants;

import com.wigroup.scheduledtasks.ClearLogsTask;

public enum Task {

    CLEAR_AUDIT_TRAIL_LOGS(ClearLogsTask.class);

    private final Class<?> clazz;

    private Task(Class<?> value) {
        this.clazz = value;
    }

    public Class<?> getClazz() {
        return clazz;
    }

}

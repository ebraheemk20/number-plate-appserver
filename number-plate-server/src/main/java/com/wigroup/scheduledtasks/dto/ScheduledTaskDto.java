package com.wigroup.scheduledtasks.dto;

import java.util.concurrent.TimeUnit;

import com.wigroup.common.Status;
import com.wigroup.scheduledtasks.entity.ScheduledTask;

public class ScheduledTaskDto {

    private Integer id;
    private String name;
    private Long frequency;
    private TimeUnit timeUnit;
    private Status status;

    public ScheduledTaskDto(ScheduledTask thisTask) {
        this.id = thisTask.getId();
        this.name = thisTask.getName();
        this.frequency = thisTask.getFrequency();
        this.timeUnit = thisTask.getTimeUnit();
        this.status = thisTask.getStatus();
    }

    public ScheduledTaskDto updateId(Integer id) {
        this.id = id;
        return this;
    }

    private ScheduledTaskDto() {
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getFrequency() {
        return frequency;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public Status getStatus() {

        return status;
    }
}

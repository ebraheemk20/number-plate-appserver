package com.wigroup.scheduledtasks.service;

import java.util.ArrayList;
import java.util.List;

import com.wigroup.scheduledtasks.entity.ScheduledTask;
import com.wigroup.scheduledtasks.dto.ScheduledTaskDto;

public class ScheduledTaskWrapper {

    private List<ScheduledTaskDto> scheduledTasks = new ArrayList<>();

    public ScheduledTaskWrapper(List<ScheduledTask> scheduledTasks) {
        scheduledTasks.forEach(task -> {
            this.scheduledTasks.add(new ScheduledTaskDto(task));
        });

    }

    private ScheduledTaskWrapper() {
    }

    public List<ScheduledTaskDto> getScheduledTasks() {
        return scheduledTasks;
    }
}

===========================================================================
Number Plate AppServer           
===========================================================================

This AppServer uses the current standard technologies used at wiGroup.

Technologies include:
- JEE 8
- Maven 

Developer setup
===============

Database
--------
Run database-config.sql located in number-plate-scripts -> server-configuration:
$ mysql -uroot -p mydatabase < database-config.sql

OR run the script in the database client of choice

Build Java code and populate database
-----------------------------------
Go to the number-plate-appserver home folder and execute with maven:

cd {workspace}/number-plate-appserver
mvn clean install -Pcreate-database

Wildfly
-------
Install Wildfly 9.0.2.Final:

wget -c http://download.jboss.org/wildfly/9.0.2.Final/wildfly-9.0.2.Final.tar.gz
mv wildfly-9.0.2.Final.tar.gz /opt
cd /opt
tar -xzvf wildfly-9.0.2.Final.tar.gz
ln -s wildfly-9.0.2.Final wildfly

OR download and install the Wildfly server if Windows.

Wildfly configuration
---------------------
Make sure Wildfly server is running (use jboss-cli.bat if Windows):

cd {workspace}/number-plate-appserver/number-plate-scripts
{wildfly_home}/bin/jboss-cli.sh --file=./target/filtered-resources/wildfly-config.cli

Personalising
-------------
Note: The following implementation is based on using Netbeans 8.1 IDE.

1) Rename the parent project: Right click -> Rename -> Check all the 
   boxes and enter your personalised projects name.
2) Open and rename the child projects (refer to step 1).
3) Open the pom.xml file of the parent project and rename the server.name, 
   script.name and wadl.name(optional) tags.
4) Open the pom.xml files of the child projects and rename the artifact id's 
   enclosed by the parent tag.
5) In the child script project's pom.xml file, specify your database details 
   in the tags enclosed by the properties tag.
6) Do the necessary changes in the database-config.sql file located 
   in the child script project. E.g: Database name.
7) Change the datasource name in the persistence.xml file (child server project).
8) Change the context-root in the jboss-web.xml file. (child server project).
9) To change the start up web page details (optional), 
   edit the index.html file located in the child server project.
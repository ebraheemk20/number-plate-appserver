-- Creates the applicaiton user. Part of the setup, and should not be included in the dbmaintain scripts
-- clearCREATE USER 'sa'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'sa'@'localhost';
FLUSH PRIVILEGES;
CREATE DATABASE number_plate;
CREATE TABLE `number_plate`.`dbmaintain_scripts` ( file_name VARCHAR(150), file_last_modified_at BIGINT, checksum VARCHAR(50), executed_at VARCHAR(20), succeeded BIGINT )
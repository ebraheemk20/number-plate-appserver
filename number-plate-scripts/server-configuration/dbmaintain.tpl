#######################################
# Default configuration of DbMaintain #
# With overridden  values for wiGroup #
#######################################

# This file contains default configuration values for dbmaintain. This file should not be edited.
# All properties in this file can be overridden, either in the project specific properties file
# (dbmaintain.properties)

### Database connections ###

# A list of logical names for the different configured databases, separated by comma's. If there's
# just one database, this property is not required. If names are listed, these names must be used
# in the names of the properties that define the database connection parameters.
databases.names=

# Properties that define the connection parameters to the database. If there are multiple databases,
# you have to specify the names of these databases in the database property names (i.e. database.dialect,
# database.driverClassName, database.url, database.userName, database.password and database.schemaNames).
# E.g. if database.names=abc,def you must define the properties database.abc.url and database.def.url.
# If there's only one database, database.names can be omitted and you simply use the property database.url.

# Comma-separated list of all database schemas used.
# A schema name is case sensitive if it's surrounded by database identifier quotes (eg " for oracle)
database.schemaNames=${MYSQL_DATABASE}
database.driverClassName=${driverClassName}
database.url=${CONNECTION_URL}
database.userName=${MYSQL_USER}
database.password=${MYSQL_PASSWORD}

# This property specifies the underlying DBMS implementation. Supported values are 'oracle', 'db2', 'mysql', 'hsqldb',
# 'postgresql', 'derby', 'mssql' and 'informix'.
# DbMaintain will try to detect the dialect from the JDBC url. You can set this property when DbMainain fails to auto-detect the dialect. |
database.dialect=mysql



### DbMaintainer configuration ###

# Indicates the database can be recreated from scratch if needed. If set to true, the database is cleared and recreated
# from scratch in case of an irregular script update. Following are irregular script updates:
# - A script that was already executed has been modified
# - A new script has been added with an index number lower than the one of an already executed script
# - An script that was already executed has been removed or renamed
#
# If set to false, the dbmaintainer will give an error if one of these situations occurs.
dbMaintainer.fromScratch.enabled=${dbmaintain.fromScratch.enabled}

# Scripts can be qualified using script qualifiers, e.g. to qualify a 01_script1.sql as 'special' rename it to
# 01_#special_script1.sql (the qualifier is not case sensitive).
dbMaintainer.qualifiers=${dbmaintain.qualifiers}

# Qualifiers can be used to include or exclude certain scripts from execution. This can be done by excluding and/or including
# certain qualifiers. Scripts qualified with an excluded qualifier are not executed. Use '<unqualified>' to indicate scripts with
# a qualifier (<unqualified> does not have to be specified as qualifier in dbMaintainer.qualifiers).
dbMaintainer.excludedQualifiers=${dbmaintain.excluded.qualifiers}
# If the includedQualifiers property is set, then a script is only executed if it contains at least on of the included
# qualifiers. Use '<unqualified>' to indicate scripts with a qualifier (<unqualified> does not have to be specified as
# qualifier in dbMaintainer.qualifiers).
dbMaintainer.includedQualifiers=${dbmaintain.included.qualifiers}

# An optional baseline revision.
# All scripts with a revision lower than this baseline revision will be ignored. No errors will be generated when
# such scripts are missing or were changed.
# This can be set when the your initial database is already in a baseline state (for example restored from an image
# of production). You can then safely remove all scripts before this baseline revision without having any errors during an update.
# When a script jar is created, it will only contain scripts starting from this baseline revision.
# In other words, it will only contain the deltas with the baseline.
dbMaintainer.baseline.revision=

# An optional properties file that contains parameter names and values. Any occurrences of parameters in the form
# ${paramName} in the database script will then be replaced by their corresponding value from the properties file.
# Parameters that are not found in the properties file are ignored.
dbMaintainer.scriptParameterFile=

# Determines how the database stores non-quoted identifiers (with identifiers, we mean names for tables, columns, etc.)
# Possible values are lower_case, upper_case, mixed_case and auto
# If auto is specified, the database metadata is used to determine the correct value
database.storedIndentifierCase.oracle=auto
database.storedIndentifierCase.hsqldb=auto
database.storedIndentifierCase.mysql=auto
database.storedIndentifierCase.db2=auto
database.storedIndentifierCase.postgresql=auto
database.storedIndentifierCase.derby=auto
database.storedIndentifierCase.mssql=auto
database.storedIndentifierCase.informix=auto

# Determines the string the database uses to quote identifiers, i.e. make them case-sensitive
# (with identifiers, we mean names for tables, columns, etc.)
# Use 'none' if quoting is not supported.
# If auto is specified, the database metadata is used to determine the correct value
database.identifierQuoteString.oracle=auto
database.identifierQuoteString.hsqldb=auto
database.identifierQuoteString.mysql=auto
database.identifierQuoteString.db2=auto
database.identifierQuoteString.postgresql=auto
database.identifierQuoteString.derby=auto
database.identifierQuoteString.mssql=auto
database.identifierQuoteString.informix=none

# Name of the table that contains the database update script that have already been executed on the database.
dbMaintainer.executedScriptsTableName=dbmaintain_scripts
# Name of the column in which the name of the executed script file is stored
dbMaintainer.fileNameColumnName=file_name
dbMaintainer.fileNameColumnSize=150
# Name of the column in which the last modification date of the executed script file is stored.
dbMaintainer.fileLastModifiedAtColumnName=file_last_modified_at
# Name of the column in which the checksum of the content of the script is stored.
dbMaintainer.checksumColumnName=checksum
dbMaintainer.checksumColumnSize=50
# Name of the column that stores the timestamp at which the script was executed
dbMaintainer.executedAtColumnName=executed_at
dbMaintainer.executedAtColumnSize=20
# Name of the column in which is stored whether the script ran without error or not.
dbMaintainer.succeededColumnName=succeeded
# If set to true, the table DBMAINTAIN_SCRIPTS will be created automatically if it does not exist yet.
# If false, an exception is thrown, indicating how to create the table manually.
dbMaintainer.autoCreateDbMaintainScriptsTable=true
dbMaintainer.timestampFormat=yyyy-MM-dd HH:mm:ss

# Comma separated list of locations where database scripts can be found. This list may contain folders as well as script archive files.
dbMaintainer.script.locations=
# Extension of the files containing the database update scripts
dbMaintainer.script.fileExtensions=sql,ddl
# Comma separated list of directories and files in which the post processing database scripts are
# located. Directories in this list are recursively search for files.
dbMaintainer.postProcessingScript.directoryName=postprocessing
# Encoding to use when reading the script files
dbMaintainer.script.encoding=${project.build.sourceEncoding}
# If true, carriage return chars will be ignored when calculating check sums.
# This will make all scripts OS independent: all line endings will be new lines (linux-style) during the check sum
# calculation. This avoids problems when copying and editing files on both Windows and Linux platforms. The check sums will
# still be the same if only the type of line endings has changed.
# By default this setting is disabled for backwards compatibility reasons.
dbMaintainer.script.ignoreCarriageReturnsWhenCalculatingCheckSum=false
# The regexp to use for locating the index part in the filename (without extension).
# The index should be in the beginning of the filename followed by a _ and should only consist of numbers.
#   E.g. 01_myscript.sql
# The regexp should contain 1 capturing group to mark the value that is to be used.
dbMaintainer.script.index.regexp=^([0-9]+)(?:_|$)
# The regexp to use for locating the target database part in the filename (without extension).
# If the file name contains multiple target database names, the last found target database is used
#   E.g. 01_@database1/01_@database2_myscript.sql
# The regexp should contain 1 capturing group to mark the value that is to be used.
dbMaintainer.script.targetDatabase.regexp=(?:\\G|_)@([a-zA-Z0-9]+)(?:_|$)
# The regexp to use for locating the qualifiers in the filename ((without extension).
# E.g. if the filename is 01_#refdata_myscript.sql, this script is qualified as a refdata script
# The regexp should contain 1 capturing group to mark the value that is to be used.
dbMaintainer.script.qualifier.regexp=(?:\\G|_)#([a-zA-Z0-9]+)(?:_|$)
# The qualifier to use to determine whether a script is a patch script.
#   E.g. 01_#patch_myscript.sql
dbMaintainer.script.patch.qualifiers=patch
# If this property is set to true, a patch script is allowed to be executed
# even if another script with a higher index was already executed.
dbMaintainer.allowOutOfSequenceExecutionOfPatches=false
# The maximum nr of chars of the script content to log when exception occurs, 0 to not log any script content
dbMaintainer.maxNrOfCharsWhenLoggingScriptContent=0

# Defines whether script last modification dates can be used to decide whether an existing script has changed. If set
# to true, the dbmaintainer will decide that a file didn't change since the last time if it's last modification date hasn't
# changed. If it did change, it will first calculate the checksum of the file to verify that the content really
# changed. Setting this property to true improves performance: if set to false the checksum of every script must
# be calculated for each run of the dbmaintainer.
dbMaintainer.useScriptFileLastModificationDates=true

# Set to true if characters can be escaped by using backslashes. For example '\'' instead of the standard SQL way ''''.
# Note this is not standard SQL behavior and is therefore disabled by default.
dbMaintainer.script.backSlashEscapingEnabled=false

# Defines the command to use for invoking oracle's SQL*Plus if the SqlPlusScriptRunner is used
dbMaintainer.sqlPlusScriptRunner.sqlPlusCommand=sqlplus
# Defines the command to use for invoking DB2's CLP if the Db2ScriptRunner is used
dbMaintainer.db2ScriptRunner.db2Command=db2

# If set to true, all foreign key and not null constraints of the database are automatically disabled before and
# after the execution of the scripts. False by default.
dbMaintainer.disableConstraints=${dbmaintain.disableConstraints}

# If set to true, all database sequences and identity columns are set to a minimum value after the execution of the scripts.
# The minimum accepted sequence value is indicated by the property sequenceUpdater.lowestAcceptableSequenceValue
dbMaintainer.updateSequences=false
# Threshold indicating the minimum value of sequences. If sequences are updated, all sequences having a lower value than this
# one are set to this value.
sequenceUpdater.lowestAcceptableSequenceValue=1000


# Indicates whether the database should be 'cleaned' before scripts are executed. If true, the
# records of all database tables, except for the ones listed in 'dbMaintainer.preserve.*' or
# 'dbMaintain.preserveDataOnly.*' are deleted before and after executing the scripts. False by default.
dbMaintainer.cleanDb=false

# Comma separated list of database items that may not be dropped or cleared by DbMaintain when
# updating the database from scratch.
# Schemas can also be preserved entirely. If identifiers are quoted (eg "" for oracle) they are considered
# case sensitive. Items may be prefixed with the schema name. Items that do not have a schema prefix are
# considered to be in the default schema.
dbMaintainer.preserve.schemas=
dbMaintainer.preserve.tables=
dbMaintainer.preserve.views=
dbMaintainer.preserve.materializedViews=
dbMaintainer.preserve.synonyms=
dbMaintainer.preserve.sequences=

# Comma separated list of table names. The tables listed here will not be emptied during a cleanDatabase operation.
# Data of the dbmaintain_scripts table is preserved automatically.
# Tables listed here will still be dropped before a fromScratch update. If this is not desirable
# you should use the property dbMaintainer.preserve.tables instead.
# Schemas can also be preserved entirely. If identifiers are quoted (eg "" for oracle) they are considered
# case sensitive. Items may be prefixed with the schema name. Items that do not have a schema prefix are considered
# to be in the default schema
dbMaintainer.preserveDataOnly.schemas=
dbMaintainer.preserveDataOnly.tables=


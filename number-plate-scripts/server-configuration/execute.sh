#!/bin/bash
# This is the docker version of the redeploy.sh script that is used on the EC2 instances
# Andrew Basson
# wiGroup DevOps
#
# Usage: execute.sh [WildFly mode] [configuration file]
#
# The default mode is 'standalone' and default configuration is based on the
# mode. It can be 'standalone.xml' or 'domain.xml'.

. /etc/profile.d/jdk.sh || { exit 1; }
. /etc/profile.d/wildfly.sh || { exit 2; }

# Use this snippet to export all variables in the sourced file
set -a && . /etc/default/wildfly; set +a # set -o allexport option

JBOSS_CLI=$JBOSS_HOME/bin/jboss-cli.sh

# Override defaults from the CLI
JBOSS_MODE=${1:-"${JBOSS_MODE}"}
JBOSS_CONFIG=${2:-"${JBOSS_CONFIG}"}

cd ${JBOSS_HOME}/customisations

function wait_for_server() {
  until `$JBOSS_CLI -c ":read-attribute(name=server-state)" 2> /dev/null | grep -q running`; do
    sleep 1
  done
}

echo "=> Starting WildFly server"
$JBOSS_HOME/bin/$JBOSS_MODE.sh -b `hostname -i` -c $JBOSS_CONFIG &
$JBOSS_HOME/bin/add-user.sh -up mgmt-users.properties ${wildfly.admin.user} ${wildfly.admin.password} --silent

echo "=> Waiting for the server to boot"
wait_for_server

echo "Starting JBOSS server..."
export CONNECTION_URL=jdbc:mysql://${MYSQL_HOST}:${MYSQL_PORT}/${MYSQL_DATABASE}
export MYSQL_USER=${MYSQL_USER}
export MYSQL_PASSWORD=${MYSQL_PASSWORD}
export CSRF_ADDR=$(/sbin/ifconfig eth0|grep "inet addr"|awk -F':' {'print $2'}|awk {'print $1'})

# Allow variables in CLI scripts
sed -i -e "s/<resolve-parameter-values>false/<resolve-parameter-values>true/" $JBOSS_HOME/bin/jboss-cli.xml

# Create CLI variables file
cat > cli.properties << EOF
database.user=${MYSQL_USER}
database.password=${MYSQL_PASSWORD}
database.host=${MYSQL_HOST}
database.port=${MYSQL_PORT}
database.schema=${MYSQL_DATABASE}
csrf.address=${CSRF_ADDR}
EOF

[ -f wildfly-config.cli  ] && { $JBOSS_CLI --properties=cli.properties --file=wildfly-config.cli;  }
[ -f wildfly-config-validate.cli  ] && { $JBOSS_CLI  --properties=cli.properties --file=wildfly-config-validate.cli;  }
/bin/rm -f cli.properties 2>/dev/null

# Needed if you are running docker
echo "=> Shutting down WildFly"
if [ "$JBOSS_MODE" = "standalone" ]; then
    $JBOSS_CLI -c ":shutdown"
else
    $JBOSS_CLI -c "/host=*:shutdown"
fi

# Update Database in debug mode
export DBMAINTAIN_OPTS=-Xdebug
export DBMAINTAIN_JDBC_DRIVER=${JBOSS_HOME}/customisations/mysql-connector-java-${mysql.connector.version}.jar

# Substitute ENV variables in the properties file
template(){
    # usage: template file.tpl
    while IFS= read -r line ; do
            line=${line//\"/\\\"}
            line=${line//\`/\\\`}
            line=${line//\$/\\\$}
            line=${line//\\\${/\${}
            eval "printf '%s\n' \"$line\"";
    done < ${1}
}

[ -f dbmaintain.tpl ] || { echo "dbmaintain.tpl not found"; exit 1; }
template "dbmaintain.tpl" > dbmaintain.properties

./dbmaintain.sh updateDatabase ${project.artifactId}.jar

# Deploy the WARs
/bin/cp -v ${JBOSS_HOME}/customisations/*.war $JBOSS_HOME/$JBOSS_MODE/deployments/

Needed if you are running docker
echo "=> Restarting WildFly on `hostname -i`"
$JBOSS_HOME/bin/$JBOSS_MODE.sh -b `hostname -i`  -bmanagement 0.0.0.0 -c $JBOSS_CONFIG


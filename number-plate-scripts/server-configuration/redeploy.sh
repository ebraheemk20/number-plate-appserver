#!/bin/bash
# This is the CI script that is used on the EC2 instances to configure and deploy the Wildfly WS
# Andrew Basson
# wiGroup DevOps
#
# Usage: redeploy
#
# The default mode is 'standalone' and default configuration is based on the
# mode. It can be 'standalone.xml' or 'domain.xml'.

. /etc/profile.d/jdk.sh || { exit 1; }
. /etc/profile.d/wildfly.sh || { exit 2; }

# Use this snippet to export all variables in the sourced file
set -a && . /etc/default/wildfly; set +a # set -o allexport option


# Override defaults from the CLI
JBOSS_MODE=${1:-"${JBOSS_MODE}"}
JBOSS_CONFIG=${2:-"${JBOSS_CONFIG}"}

JBOSS_CLI=$JBOSS_HOME/bin/jboss-cli.sh
JBOSS_DEPLOY=$JBOSS_HOME/$JBOSS_MODE/deployments/

# Keep eyes on
env >/tmp/env.list

cd ${JBOSS_HOME}/customisations

function wait_for_server() {
    until `$JBOSS_CLI -c ":read-attribute(name=server-state)" 2> /dev/null | grep -q running`; do
        sleep 1
    done
}

template(){
    # Substitute ENV variables in the dbmaintain template file to create the properties file

    # usage: template file.tpl
    while IFS= read -r line ; do
            line=${line//\"/\\\"}
            line=${line//\`/\\\`}
            line=${line//\$/\\\$}
            line=${line//\\\${/\${}
            eval "printf '%s\n' \"$line\"";
    done < ${1}
}

config_wildfly(){

      # Allow variables in CLI scripts
    sed -i -e "s/<resolve-parameter-values>false/<resolve-parameter-values>true/" $JBOSS_HOME/bin/jboss-cli.xml
    CSRF_ADDR=$(/sbin/ifconfig eth0|grep "inet addr"|awk -F':' {'print $2'}|awk {'print $1'})

    [ -f cli.properties ] || {
    # Create the CLI variables file
    cat - > cli.properties << EOF
database.user=${MYSQL_USER}
database.password=${MYSQL_PASSWORD}
database.host=${MYSQL_HOST}
database.port=${MYSQL_PORT}
database.schema=${MYSQL_DATABASE}
csrf.address=${CSRF_ADDR}
EOF

    }

    [ -f wildfly-config.cli  ] && { $JBOSS_CLI --properties=cli.properties --file=wildfly-config.cli;  }
    [ -f wildfly-config-validate.cli  ] && { $JBOSS_CLI  --properties=cli.properties --file=wildfly-config-validate.cli;  }
    # cli.properties 2>/dev/null

}

stop_auto_deploy(){
    # Disable Auto-Deploy
    cd $JBOSS_DEPLOY
    for WAR in `ls -1 *.war`; do
       touch ${WAR}.skipdeploy
    done
    cd -
    # Or do we want to force an undeploy?
}

config_database(){

    # /etc/sshd/ssh_config : AcceptEnv MYSQL_* to receive EnvVars
    echo "Starting JBOSS server..."
    export CONNECTION_URL=jdbc:mysql://${MYSQL_HOST}:${MYSQL_PORT}/${MYSQL_DATABASE}
    export MYSQL_USER=${MYSQL_USER}
    export MYSQL_PASSWORD=${MYSQL_PASSWORD}

    # Update Database in debug mode
    export DBMAINTAIN_OPTS=-Xdebug
    #export JDBC_DRIVER=com.mysql.driver
    export DBMAINTAIN_JDBC_DRIVER=${JBOSS_HOME}/customisations/mysql-connector-java-${mysql.connector.version}.jar

    [ -f dbmaintain.tpl ] || { echo "dbmaintain.tpl not found"; exit 1; }
    [ -f dbmaintain.properties ] || { template "dbmaintain.tpl" > dbmaintain.properties; }

    # updateDatabase
    ./dbmaintain.sh updateDatabase ${project.artifactId}.jar

}

deploy_wildfly(){
    # Deploy the WARs against the newly updated DB
    for WAR in `ls -1 *.war`; do
        /bin/cp -vf ${WAR} $JBOSS_DEPLOY/
        rm -f ${JBOSS_DEPLOY}/${WAR}.skipdeploy 2>/dev/null
        touch ${JBOSS_DEPLOY}/${WAR}.dodeploy
    done
}

echo "=> Waiting for access to the container service"
wait_for_server

config_wildfly
stop_auto_deploy
config_database
deploy_wildfly

# TODO - cli change that requires a WF restart?
# TODO - Stop service for db maintenance?
# TODO - Restart

# Done
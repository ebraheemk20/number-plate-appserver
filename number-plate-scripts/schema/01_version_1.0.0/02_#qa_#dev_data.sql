INSERT INTO api (id, api_key) VALUES
  ('ADMIN', '4622c889-63c5-4636-87dc-49f257220893'),
  ('IPHONE', 'a73ce251-b3b5-46e6-9c91-27e12e98cc5a'),
  ('ANDROID', '437ac227-8afa-4647-926d-265c9b05f0ae');

INSERT INTO `config` (id, description, `value`, type, date_created) VALUES
('AUDIT_TRAIL_RETENTION_MONTH','The number of months that extra data is kept in audit trails before being removed.','2','INTEGER',NOW()),
('CONFIG_SERVER_URL','Config Server URL','http://rad2.wigroup.co:8080/wigroup-config/vsp/','STRING',NOW()),
('DEFAULT_PAGE_SIZE','Default page size.','10','INTEGER',NOW()),
('LOG_RESPONSE_REQUESTS','Set this to true if requests and responses should be logged','true','BOOLEAN',NOW()),
('SMS_GATEWAY_CLIENT_ID','SMS gateway client Id','20051','STRING',NOW()),
('SMS_GATEWAY_CLIENT_IS_SSL','Whether SMS gateway is SSL enabled or not','FALSE','BOOLEAN',NOW()),
('SMS_GATEWAY_CLIENT_PASSWORD','SMS gateway client password','test','STRING',NOW()),
('SMS_GATEWAY_IP','SMS gateway IP','qa.wigroup.co','STRING',NOW()),
('SMS_GATEWAY_PORT','SMS gateway Port','41008','INTEGER',NOW()),
('SMS_GATEWAY_TIMEOUT_MS','SMS gateway socket connection timeout in MS','10000','INTEGER',NOW()),
('CONN_TIMEOUT_MSEC','Connection timeout in msec','5000','INTEGER',NOW()),
('SOCKET_TIMEOUT_MSEC','Socket timeout in msec','10000','INTEGER',NOW()),
('SERVER_URL', 'The base url of the server', 'http://pocketflo-dev.wigroup.co:8080/pocketflo-server/', 'STRING', NOW()),
('SESSION_TIMEOUT_SECONDS', 'Subscriber session timeout in seconds (0 immediate timeout!)', '7776000', 'INTEGER', NOW()),
('SERVER_NAME', 'Name of the server', 'PocketFlo' , 'STRING', NOW()),
('AWS_ACCESS_KEY', 'amazon ses key', 'AKIAJVJVR6IOS4DWZNNA', 'STRING', NOW()),
('AWS_SECRET_KEY', 'Amazon ses key', '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', 'STRING', NOW());


INSERT INTO `scheduled_task` (name, frequency, time_unit, date_created, status) VALUES
('CLEAR_AUDIT_TRAIL_LOGS', 3, 'DAYS', NOW(), 'ACTIVE');


INSERT INTO message (id, date_created, last_updated, message, code, http_status_code) values
('DEVICE_PLATFORM_VALIDATION', NOW(), NOW(), 'Both deviceId and platform are required fields.', '5000035', NULL),
('FIELD_LENGTH', NOW(), NOW(), 'Length exceeds the allowed limit.', '5000027', 422),
('ID_NUMBER_VALIDATION', NOW(), NOW(), 'ID Number is invalid.', '5000029', 400),
('LOGGED_IN', NOW(), NOW(), 'Logged in.', '5000032', 200),
('OTP_NOT_YET_VALIDATED', NOW(), NOW(), 'OTP has not yet been validated.', '5000033', 400),
('PIN_ALREADY_UPDATED', NOW(), NOW(), 'Pin has already been updated.','5000036', '400'),
('VERIFICATION_EMAIL_EXPIRED', NOW(), NOW(), 'Verification email as expired.', '5000025', 400),
('VERIFICATION_EMAIL_SENT', NOW(),NOW(),'Verification email sent.','5000026', 200),
('EMAIL_REQUEST_PROCESSED', NOW(),NOW(),'Email request processed.','5000028', 200);




